using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace MyGizmoVisma
{
    public partial class PathSettings : Form
    {
        public String pftg;
        public String psys;

        public PathSettings()
        {
            pftg = "";
            psys = "";

            RegistryKey key = Registry.LocalMachine.OpenSubKey("Software\\Microsoft\\Windows\\CurrentVersion\\App Paths\\SpcsAdm.exe");
            try
            {
                psys = Convert.ToString(key.GetValue("CommonFiles"));
                pftg = Convert.ToString(key.GetValue("DefaultCompanyPath"));
                pftg += @"\Ovnbol2000";
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                File.AppendAllText("log.txt", DateTime.Now + " " + ex + Environment.NewLine +
       Environment.NewLine);
            }
            key.Close();

            InitializeComponent();
        }

        private void PATH_BN_OPEN_Click(object sender, EventArgs e)
        {
            pftg = this.PathsFtg.Text;
            psys = this.PathsSys.Text;

            this.Close();
        }
    }
}