﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdkNet4Wrapper;
using System.IO;

namespace MyGizmoVisma
{
    class CreateOrUpdateProjects
    {
        private int pData;
        private Api.ADKERROR error;
        public void CreateOrUpdate(List<Projects> projects, string tempFolderPath, Dictionary<string, string> CustomerDictionary)
        {
            foreach (Projects project in projects)
            {                
                pData = Api.AdkCreateData(Api.ADK_DB_PROJECT);
                string id = project.id.ToString();
                error = Api.AdkSetStr(pData, Api.ADK_PROJECT_CODE_OF_PROJECT, ref id);

                string ProjectName = "";
                if (project.ProjectName != null)
                {
                    ProjectName = project.ProjectName;
                    if (ProjectName.Count() > 50)
                    {
                        ProjectName = ProjectName.Substring(50);
                    }                    
                    error = Api.AdkSetStr(pData, Api.ADK_PROJECT_NAME, ref ProjectName);
                    CheckError("ProjectName: " + ProjectName, error, tempFolderPath);
                    
                }

                if(CustomerDictionary.ContainsKey(project.CustomerID))
                {
                    string CustomerNumber = CustomerDictionary[project.CustomerID];
                    error = Api.AdkSetStr(pData, Api.ADK_PROJECT_CUSTOMER_NUMBER, ref CustomerNumber);
                    CheckError("CustomerNumber - ProjectName: " + ProjectName, error, tempFolderPath);
                }
                
                                
                if (project.StartDate != null)
                {
                    string StartDate = project.StartDate.ToString("yyyy-MM-dd");
                    int StartDateInt = 0;
                    error = Api.AdkDateToLong(ref StartDate, ref StartDateInt);
                    error = Api.AdkSetDate(pData, Api.ADK_PROJECT_DATE_OF_BEGINNING, StartDateInt);

                    CheckError("StartDate - ProjectName: " + ProjectName, error, tempFolderPath);
                }

                if (project.EndDate != null)
                {
                    string EndDate = project.StartDate.ToString("yyyy-MM-dd");
                    int EndDateInt = 0;
                    error = Api.AdkDateToLong(ref EndDate, ref EndDateInt);
                    error = Api.AdkSetDate(pData, Api.ADK_PROJECT_DATE_OF_END, EndDateInt);

                    CheckError("EndDate - ProjectName: " + ProjectName, error, tempFolderPath);
                }

                Api.AdkAdd(pData);
                CheckError("ProjectName: " + ProjectName, error, tempFolderPath);
                Api.AdkDeleteStruct(pData);
            }
        }


        public static void CheckError(string id, Api.ADKERROR error, string tempFolderPath)
        {
            if (error.lRc != Api.ADKE_OK)
            {
                String errortext = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                Console.WriteLine(errortext + " " + id);
                File.AppendAllText(tempFolderPath + "ImportLog.txt", DateTime.Now + " " + errortext + " " + id + Environment.NewLine +
       Environment.NewLine);

            }
        }
    }
}
