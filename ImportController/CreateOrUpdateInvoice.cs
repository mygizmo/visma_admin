﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdkNet4Wrapper;
using System.IO;
using Newtonsoft.Json;

namespace MyGizmoVisma
{
    class CreateOrUpdateInvoice
    {
        private Api.ADKERROR error;
        Boolean delivered_is_changed;
        Boolean delivered_note_is_printed;
        public List<invoices> CreateOrUpdate(List<invoices> invoices, string tempFolderPath, 
                    System.Windows.Forms.CheckBox checkBoxUpdateEvenIfDelivered, 
                    System.Windows.Forms.CheckBox checkBoxUpdateEvenIfDocumentPrinted,
                    System.Windows.Forms.CheckBox checkBoxUpdateYourRefWithProjectName,
                    System.Windows.Forms.CheckBox checkBoxCopyProjectDescriptionAsText)
        {
            File.AppendAllText(tempFolderPath + "ImportLog.txt", Environment.NewLine + "Import av fakturaunderlag" + Environment.NewLine);
            File.AppendAllText(tempFolderPath + "ImportLog.txt", "=========================" + Environment.NewLine);
            List<invoices> invoicesLeft = new List<invoices>();

            foreach (invoices invoicesObject in invoices)
            {
                delivered_is_changed = false;
                delivered_note_is_printed = false;
                File.AppendAllText(tempFolderPath + "ImportLog.txt", "Kund: " + invoicesObject.invoiceHead.ErpCustomerID +  ", " 
                    + invoicesObject.invoiceHead.CustomerName + Environment.NewLine);
                File.AppendAllText(tempFolderPath + "ImportLog.txt", "Ordernummer: " + invoicesObject.invoiceHead.ErpOrderNumber + Environment.NewLine);
                File.AppendAllText(tempFolderPath + "ImportLog.txt", "Fakturaunderlag: #" + invoicesObject.invoiceHead.InvoiceID + Environment.NewLine);

                int pDataOrder = Api.AdkCreateData(Api.ADK_DB_ORDER_HEAD);
                string CustomerNumberInVisma = "";
                int OrderrNumberInMyGizmo = 0;
                if (invoicesObject.invoiceHead.ErpOrderNumber != "")
                {
                    try
                    {
                        OrderrNumberInMyGizmo = Convert.ToInt32(invoicesObject.invoiceHead.ErpOrderNumber);
                    }
                    catch (Exception ex)
                    {                    
                        Console.WriteLine(ex.ToString());
                        File.AppendAllText(tempFolderPath + "ImportLog.txt", ex + Environment.NewLine);
                    }
                }
                int pDataOrderReturn = GetOrder(OrderrNumberInMyGizmo, tempFolderPath,
                            checkBoxUpdateEvenIfDelivered, checkBoxUpdateEvenIfDocumentPrinted);                
                if (pDataOrderReturn != 0)
                {
                    pDataOrder = pDataOrderReturn;
                }
                else
                {
                    pDataOrder = Api.AdkCreateData(Api.ADK_DB_ORDER_HEAD);
                }

                if (invoicesObject.invoiceHead.ErpCustomerID != null && invoicesObject.invoiceHead.ErpCustomerID != "")
                {
                    string ErpCustomerID = invoicesObject.invoiceHead.ErpCustomerID;
                    error = Api.AdkSetStr(pDataOrder, Api.ADK_OOI_HEAD_CUSTOMER_NUMBER, ref ErpCustomerID);
                    CustomerNumberInVisma = "" + ErpCustomerID;
                    CheckError("CustomerNumber : " + CustomerNumberInVisma, error, tempFolderPath);                    
                }               

                if (invoicesObject.invoiceHead.ErpProjectID != null && invoicesObject.invoiceHead.ErpProjectID != "")
                {
                    string ErpProjectID = invoicesObject.invoiceHead.ErpProjectID;
                    error = Api.AdkSetStr(pDataOrder, Api.ADK_OOI_HEAD_PROJECT_CODE, ref ErpProjectID);
                    CheckError("ProjectID Ordernr: " + CustomerNumberInVisma, error, tempFolderPath);
                }

                if (invoicesObject.invoiceHead.ProjectName != null && invoicesObject.invoiceHead.ProjectName != "")
                {
                    if (checkBoxUpdateYourRefWithProjectName.Checked)
                    {
                        string ProjectName = invoicesObject.invoiceHead.ProjectName;
                        error = Api.AdkSetStr(pDataOrder, Api.ADK_OOI_HEAD_CUSTOMER_REFERENCE_NAME, ref ProjectName);
                        CheckError("ProjectID Ordernr: " + CustomerNumberInVisma, error, tempFolderPath);
                    }
                }

                if (invoicesObject.invoiceHead.Date != null)
                {
                    string OrderDate = invoicesObject.invoiceHead.Date.ToString("yyyy-MM-dd");
                    int OrderDateInt = 0;
                    error = Api.AdkDateToLong(ref OrderDate, ref OrderDateInt);
                    error = Api.AdkSetDate(pDataOrder, Api.ADK_OOI_HEAD_DOCUMENT_DATE1, OrderDateInt);
                    CheckError("OrderDate Ordernr: " + CustomerNumberInVisma, error, tempFolderPath);
                }

                // Check number of existing lines
                double NoOfRows = 0;
                if (pDataOrderReturn != 0)
                {
                    error = Api.AdkGetDouble(pDataOrder, Api.ADK_OOI_HEAD_NROWS, ref NoOfRows);
                    // Check our and customer ref
                    String CustomerRef = new String(' ', 50);
                    error = Api.AdkGetStr(pDataOrder, Api.ADK_OOI_HEAD_CUSTOMER_REFERENCE_NAME, ref CustomerRef, 50);
                    CheckError("CustomerRef Ordernr: " + CustomerNumberInVisma, error, tempFolderPath);
                    File.AppendAllText(tempFolderPath + "ImportLog.txt", "CustomerRef: " + CustomerRef + Environment.NewLine);
                    String OurRef = new String(' ', 25);
                    error = Api.AdkGetStr(pDataOrder, Api.ADK_OOI_HEAD_OUR_REFERENCE_NAME, ref OurRef, 25);
                    CheckError("OurRef Ordernr: " + CustomerNumberInVisma, error, tempFolderPath);
                    File.AppendAllText(tempFolderPath + "ImportLog.txt", "OurRef: " + OurRef + Environment.NewLine);
                }

                int nDescriptionRows = 0;
                // Check if copy project description as text on invoice
                if (checkBoxCopyProjectDescriptionAsText.Checked)
                {
                    if (invoicesObject.invoiceHead.ProjectDescription != null && invoicesObject.invoiceHead.ProjectDescription != "")
                    {
                        string ProjectDescriptionText = invoicesObject.invoiceHead.ProjectDescription;
                        nDescriptionRows = ((ProjectDescriptionText.Count() + 59) / 60);
                    }
                }

                int nrows = 0;
                if (invoicesObject.invoiceLines.notes != null)
                {
                    nrows = nDescriptionRows + Convert.ToInt32(NoOfRows) + invoicesObject.invoiceLines.notes.Count() + invoicesObject.invoiceLines.products.Count();
                }
                else
                {
                    nrows = nDescriptionRows + Convert.ToInt32(NoOfRows) + invoicesObject.invoiceLines.products.Count();
                }

                int pRows = Api.AdkCreateDataRow(Api.ADK_DB_INVOICE_ROW, nrows);
                CheckError("Get rows Ordernr: " + CustomerNumberInVisma, error, tempFolderPath);
                
                int i = Convert.ToInt32(NoOfRows);

                // Check if copy project description as text on invoice
                if (checkBoxCopyProjectDescriptionAsText.Checked)
                {
                    if (invoicesObject.invoiceHead.ProjectDescription != null && invoicesObject.invoiceHead.ProjectDescription != "")
                    {
                        string projectDescription = invoicesObject.invoiceHead.ProjectDescription;
                        int startPos = 0;
                        int length = 60;
                        if ((startPos + length) > projectDescription.Count())
                        {
                            length = projectDescription.Count() - startPos;
                        }
                        string tempString = "";
                        for (int j = 0; j < nDescriptionRows; j++)
                        {
                            tempString = projectDescription.Substring(startPos, length);
                            int pRow = Api.AdkGetDataRow(pRows, i);
                            CheckError("Get row Ordernr: " + CustomerNumberInVisma, error, tempFolderPath);
                            string TextRow = "T";
                            error = Api.AdkSetStr(pRow, Api.ADK_OOI_ROW_TYPE_OF_ROW, ref TextRow);
                            CheckError("TextRow Title: " + tempString, error, tempFolderPath);
                            error = Api.AdkSetStr(pRow, Api.ADK_OOI_ROW_TEXT, ref tempString);
                            CheckError("Article text ProductName: " + tempString, error, tempFolderPath);
                            i++;
                            startPos = startPos + 60;
                            if ((startPos + length) > projectDescription.Count())
                            {
                                length = projectDescription.Count()- startPos;
                            }
                        }
                    }
                }
                // Notes
                if (invoicesObject.invoiceLines.notes != null)
                {
                    foreach (ILnotes note in invoicesObject.invoiceLines.notes)
                    {
                        int pRow = Api.AdkGetDataRow(pRows, i);
                        CheckError("Get row Ordernr: " + CustomerNumberInVisma, error, tempFolderPath);
                        if (note.TextRow == "true")
                        {
                            string TextRow = "T";
                            error = Api.AdkSetStr(pRow, Api.ADK_OOI_ROW_TYPE_OF_ROW, ref TextRow);
                            CheckError("TextRow Title: " + note.Title, error, tempFolderPath);

                            if (note.NoteText != null && note.NoteText != "")
                            {
                                string Text = note.NoteText;
                                if (note.NoteText.Count() > 60)
                                {
                                    Text = note.NoteText.Substring(0, 60);
                                }
                                error = Api.AdkSetStr(pRow, Api.ADK_OOI_ROW_TEXT, ref Text);
                                CheckError("Article text ProductName: " + note.NoteText, error, tempFolderPath);
                            }
                        }
                        i++;
                    }
                }

                // Products
                foreach (ILproducts product in invoicesObject.invoiceLines.products)
                {
                    int pRow = Api.AdkGetDataRow(pRows, i);
                    CheckError("Get row Ordernr: " + CustomerNumberInVisma, error, tempFolderPath);                  
                    

                    if (product.TextRow == "true")
                    {
                        string TextRow = "T";
                        error = Api.AdkSetStr(pRow, Api.ADK_OOI_ROW_TYPE_OF_ROW, ref TextRow);
                        CheckError("TextRow ProductName: " + product.ProductName, error, tempFolderPath);
                        
                        if (product.ProductName != null && product.ProductName != "")
                        {
                            string Name = product.ProductName;
                            if (product.ProductName.Count() > 60)
                            {
                                Name = product.ProductName.Substring(0, 60);
                            }
                            error = Api.AdkSetStr(pRow, Api.ADK_OOI_ROW_TEXT, ref Name);
                            CheckError("Article text ProductName: " + product.ProductName, error, tempFolderPath);
                        }

                    }
                    else
                    {
                        if (product.ErpProductID != null && product.ErpProductID != "")
                        {
                            string ErpProductID = product.ErpProductID;
                            error = Api.AdkSetStr(pRow, Api.ADK_OOI_ROW_ARTICLE_NUMBER, ref ErpProductID);
                            CheckError("Article number ProductName: " + product.ProductName, error, tempFolderPath);
                        } else
                        {
                            if (product.UnitName != null && product.UnitName != "")
                            {
                                string UnitName = product.UnitName;
                                error = Api.AdkSetStr(pRow, Api.ADK_OOI_ROW_UNIT, ref UnitName);
                                CheckError("Article text UnitName: " + product.UnitName, error, tempFolderPath);
                            }
                        }

                        if (product.ProductDescription != null && product.ProductDescription != "")
                        {
                            string Name = product.ProductDescription;
                            if (product.ProductDescription.Count() > 60)
                            {
                                Name = product.ProductDescription.Substring(0, 60);
                            }
                            error = Api.AdkSetStr(pRow, Api.ADK_OOI_ROW_TEXT, ref Name);
                            CheckError("Article text ProductName: " + product.ProductName, error, tempFolderPath);
                        }
                        else
                        {
                            if (product.ProductName != null && product.ProductName != "")
                            {
                                string Name = product.ProductName;
                                if (product.ProductName.Count() > 60)
                                {
                                    Name = product.ProductName.Substring(0, 60);
                                }
                                error = Api.AdkSetStr(pRow, Api.ADK_OOI_ROW_TEXT, ref Name);
                                CheckError("Article text ProductName: " + product.ProductName, error, tempFolderPath);
                            }
                        }

                        if (product.Date != null)
                        {
                            string ProductDate = product.Date.ToString("yyyy-MM-dd");
                            int ProductDateInt = 0;
                            error = Api.AdkDateToLong(ref ProductDate, ref ProductDateInt);
                            error = Api.AdkSetDate(pRow, Api.ADK_OOI_ROW_DATE2, ProductDateInt);
                            CheckError("Article Date ProductName: " + product.ProductName, error, tempFolderPath);
                        }

                        if (product.Quantity != null && product.Quantity != "")
                        {
                            string Qty = product.Quantity.Replace(".", ",");
                            double QtyDouble = 0;
                            Double.TryParse(Qty, out QtyDouble);
                            error = Api.AdkSetDouble(pRow, Api.ADK_OOI_ROW_QUANTITY1, QtyDouble);
                            CheckError("Article Qty1 ProductName: " + product.ProductName, error, tempFolderPath);

                            error = Api.AdkSetDouble(pRow, Api.ADK_OOI_ROW_QUANTITY2, QtyDouble);
                            CheckError("Article Qty2 ProductName: " + product.ProductName, error, tempFolderPath);
                        }

                        if (product.Price != null && product.Price != "")
                        {
                            string Price = product.Price.Replace(".", ",");
                            double PriceDouble = 0;
                            Double.TryParse(Price, out PriceDouble);
                            error = Api.AdkSetDouble(pRow, Api.ADK_OOI_ROW_PRICE_EACH_CURRENT_CURRENCY, PriceDouble);
                            CheckError("Article Price ProductName: " + product.ProductName, error, tempFolderPath);
                        }
                    }
                    i++;
                }

                error = Api.AdkSetData(pDataOrder, Api.ADK_OOI_HEAD_ROWS, pRows);
                CheckError("Ordernr: " + CustomerNumberInVisma, error, tempFolderPath);

                error = Api.AdkSetDouble(pDataOrder, Api.ADK_OOI_HEAD_NROWS, nrows);
                CheckError("Ordernr: " + CustomerNumberInVisma, error, tempFolderPath);

                if (pDataOrderReturn != 0) 
                {
                    error = Api.AdkUpdate(pDataOrder);
                    CheckError(" " + CustomerNumberInVisma, error, tempFolderPath);
                    if (error.lRc == Api.ADKE_OK)
                    {
                        if (checkBoxUpdateEvenIfDocumentPrinted.Checked)
                        {
                            if (delivered_note_is_printed)
                            {
                                int DeliverryNoteIsPrinted = 1;
                                error = Api.AdkSetBool(pDataOrder, Api.ADK_OOI_HEAD_DOCUMENT_PRINTED, DeliverryNoteIsPrinted);
                                CheckError(" " + CustomerNumberInVisma, error, tempFolderPath);
                                error = Api.AdkUpdate(pDataOrder);
                                CheckError(" " + CustomerNumberInVisma, error, tempFolderPath);
                                //File.AppendAllText(tempFolderPath + "ImportLog.txt", "Ordernerkännande har ändrats tillbaka till utskriven!!" + Environment.NewLine);
                            }
                        }
                        if (checkBoxUpdateEvenIfDelivered.Checked)
                        {
                            if (delivered_is_changed)
                            {
                                int Delivered = 1;
                                error = Api.AdkSetBool(pDataOrder, Api.ADK_OOI_HEAD_ORDER_DELIVERED, Delivered);
                                CheckError(" " + CustomerNumberInVisma, error, tempFolderPath);
                                error = Api.AdkUpdate(pDataOrder);
                                CheckError(" " + CustomerNumberInVisma, error, tempFolderPath);
                                //File.AppendAllText(tempFolderPath + "ImportLog.txt", "Levererad har ändrats tillbaka till levererad!!" + Environment.NewLine);
                            }
                        }
                        File.AppendAllText(tempFolderPath + "ImportLog.txt", "Adderat till befintlig order" + Environment.NewLine);
                    }
                    else
                    {
                        invoicesLeft.Add(invoicesObject);
                    }
                }
                else
                {
                    error = Api.AdkAdd(pDataOrder);
                    CheckError(" " + CustomerNumberInVisma, error, tempFolderPath);
                    if (error.lRc == Api.ADKE_OK)
                    {
                        File.AppendAllText(tempFolderPath + "ImportLog.txt", "Skapat ny order" + Environment.NewLine);
                    }
                    else
                    {
                        invoicesLeft.Add(invoicesObject);
                    }
                }

                Double OrderNr = 0;
                error = Api.AdkGetDouble(pDataOrder, Api.ADK_OOI_HEAD_DOCUMENT_NUMBER, ref OrderNr);
                CheckError("OrderNr: " + OrderNr, error, tempFolderPath);
                Api.AdkDeleteStruct(pDataOrder);
                File.AppendAllText(tempFolderPath + "ImportLog.txt", Environment.NewLine);
            }
            return invoicesLeft;
        }

        public int GetOrder(int OrderNr, string tempFolderPath, 
                System.Windows.Forms.CheckBox checkBoxUpdateEvenIfDelivered, System.Windows.Forms.CheckBox checkBoxUpdateEvenIfDocumentPrinted)
        {
            int pDataFind = 0;
            if (OrderNr != 0)
            {
                Api.ADKERROR error;                
                double orderNrInput = OrderNr;
                double OrderNrResult = 0;
                pDataFind = Api.AdkCreateData(Api.ADK_DB_ORDER_HEAD);
                error = Api.AdkSetDouble(pDataFind, Api.ADK_OOI_HEAD_DOCUMENT_NUMBER, orderNrInput);
                error = Api.AdkFind(pDataFind);
                if (error.lRc != Api.ADKE_OK)
                {
                    return 0;
                }
                    Api.AdkGetDouble(pDataFind, Api.ADK_OOI_HEAD_DOCUMENT_NUMBER, ref OrderNrResult);
                if (error.lRc != Api.ADKE_OK)
                {
                    String errortext = new String(' ', 200);
                    int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                    Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                    pDataFind = Api.AdkCreateData(Api.ADK_DB_ORDER_HEAD);
                }
                else if (orderNrInput == OrderNrResult)
                {
                    if (checkBoxUpdateEvenIfDelivered.Checked)
                    {
                        // Check if Delivered
                        int Delivered = 0;
                        Api.AdkGetBool(pDataFind, Api.ADK_OOI_HEAD_ORDER_DELIVERED, ref Delivered);
                        if (Delivered == 1)
                        {
                           // File.AppendAllText(tempFolderPath + "ImportLog.txt", "Ordern är redan levererad!!" + Environment.NewLine);
                            Delivered = 0;
                            error = Api.AdkSetBool(pDataFind, Api.ADK_OOI_HEAD_ORDER_DELIVERED, Delivered);
                            CheckError(" " + orderNrInput, error, tempFolderPath);
                            delivered_is_changed = true;
                            error = Api.AdkUpdate(pDataFind);
                            CheckError(" " + orderNrInput, error, tempFolderPath);
                            if (error.lRc == Api.ADKE_OK)
                            {
                                //File.AppendAllText(tempFolderPath + "ImportLog.txt", "Levererad har ändrats till icke levererad!!" + Environment.NewLine);
                            }
                        }
                    }
                    if (checkBoxUpdateEvenIfDocumentPrinted.Checked)
                    {
                        // Check if DeliverryNoteIsPrinted
                        int DeliverryNoteIsPrinted = 0;
                        Api.AdkGetBool(pDataFind, Api.ADK_OOI_HEAD_DOCUMENT_PRINTED, ref DeliverryNoteIsPrinted);
                        if (DeliverryNoteIsPrinted == 1)
                        {
                            //File.AppendAllText(tempFolderPath + "ImportLog.txt", "Ordernerkännande är redan utskriven!!" + Environment.NewLine);
                            DeliverryNoteIsPrinted = 0;
                            error = Api.AdkSetBool(pDataFind, Api.ADK_OOI_HEAD_DOCUMENT_PRINTED, DeliverryNoteIsPrinted);
                            CheckError(" " + orderNrInput, error, tempFolderPath);
                            delivered_note_is_printed = true;
                            error = Api.AdkUpdate(pDataFind);
                            CheckError(" " + orderNrInput, error, tempFolderPath);
                            if (error.lRc == Api.ADKE_OK)
                            {
                                //File.AppendAllText(tempFolderPath + "ImportLog.txt", "Ordernerkännande har ändrats till icke utskriven!!" + Environment.NewLine);
                            }
                        }
                    }
                }
                else
                {
                    pDataFind = Api.AdkCreateData(Api.ADK_DB_ORDER_HEAD);
                }
                //pDataFind = Api.AdkCreateData(Api.ADK_DB_ORDER_HEAD);
                
            }
            return pDataFind;
        }



        public static void CheckError(string id, Api.ADKERROR error, string tempFolderPath)
        {
            if (error.lRc != Api.ADKE_OK)
            {
                String errortext = new String(' ', 200);
                String errortextTable = new String(' ', 200);
                String errortextField = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                Console.WriteLine(errortext + " " + id);
                errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elDbTable;
                Api.AdkGetErrorText(ref error, errtype, ref errortextTable, 200);
                Console.WriteLine(errortextTable + " " + id);
                errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elField;
                Api.AdkGetErrorText(ref error, errtype, ref errortextField, 200);
                Console.WriteLine(errortextField + " " + id);
                File.AppendAllText(tempFolderPath + "ImportLog.txt", "Error: " + errortext + " " + id + Environment.NewLine);
                File.AppendAllText(tempFolderPath + "ImportLog.txt", "Error tabell: " + errortextTable + Environment.NewLine);
                File.AppendAllText(tempFolderPath + "ImportLog.txt", "Error fält: " + errortextField + Environment.NewLine);

            }
        }

    }
}
