﻿using System;
using System.Collections.Generic;
using System.Linq;
using AdkNet4Wrapper;
using System.IO;

namespace MyGizmoVisma
{

    class CreateOrUpdateArticles
    {
        private int pData;
        private Api.ADKERROR error;
        public List<Products> CreateOrUpdate(List<Products> products, string tempFolderPath)
        {
            File.AppendAllText(tempFolderPath + "ImportLog.txt", Environment.NewLine + "Import av artiklar" + Environment.NewLine);
            File.AppendAllText(tempFolderPath + "ImportLog.txt", "==================" + Environment.NewLine);
            List<Products> productsLeft = new List<Products>();

            foreach (Products product in products)
            {

                pData = Api.AdkCreateData(Api.ADK_DB_ARTICLE);

                string articleNumber = "";
                if (product.ErpProductID != null)
                {
                    articleNumber = product.ErpProductID;
                    File.AppendAllText(tempFolderPath + "ImportLog.txt", "Artikel: " + product.ErpProductID + ", "
                        + product.ProductName + Environment.NewLine);
                    error = Api.AdkSetStr(pData, Api.ADK_ARTICLE_NUMBER, ref articleNumber);
                    CheckError("ArtikelNumber " + articleNumber, error, tempFolderPath);
                }
                else
                {
                    int length = product.ProductName.Length;
                    if (length <= 14)
                    {
                        articleNumber = product.ProductName;
                        File.AppendAllText(tempFolderPath + "ImportLog.txt", "Artikel: " + articleNumber + ", "
                            + product.ProductName + Environment.NewLine);
                        error = Api.AdkSetStr(pData, Api.ADK_ARTICLE_NUMBER, ref articleNumber);
                        CheckError("ArtikelNumber " + articleNumber, error, tempFolderPath);
                    }
                    else
                    {
                        File.AppendAllText(tempFolderPath + "ImportLog.txt", "MyGizmo Produkt namn (" + product.ProductName + ") > 14 tecken. Kan inte importeras" + Environment.NewLine);
                    }
                }

                string articleName = "";
                if (product.ProductName != null)
                {
                    articleName = product.ProductName;
                    error = Api.AdkSetStr(pData, Api.ADK_ARTICLE_NAME, ref articleName);
                    CheckError("ArtikelName " + articleName, error, tempFolderPath);
                }

                if (product.UnitName != null)
                {
                    if (GetUnit(product.UnitName) && product.UnitName.Count()<=4)
                    {
                        string unitName = product.UnitName;
                        error = Api.AdkSetStr(pData, Api.ADK_ARTICLE_UNIT_CODE, ref unitName);
                        CheckError("UnitName ProductName: " + articleName, error, tempFolderPath);
                    }
                }

                if (product.ProductTypeName != null)
                {
                    if (GetArticleGroup(product.ProductTypeName) && product.ProductTypeName.Count() <= 6)
                    {
                        string productTypeName = product.ProductTypeName;
                        error = Api.AdkSetStr(pData, Api.ADK_ARTICLE_GROUP, ref productTypeName);
                        CheckError("ProductType ProductName: " + articleName, error, tempFolderPath);
                    }
                 }

                if (product.Price != null)
                {
                    string Price = product.Price.Replace(".", ","); ;
                    double PriceDouble = 0;
                    Double.TryParse(Price, out PriceDouble);

                    error = Api.AdkSetDouble(pData, Api.ADK_ARTICLE_PRICE, PriceDouble);
                    CheckError("Price ProductName: " + articleName, error, tempFolderPath);
                }

                if (product.ErpProductID != null && product.ErpProductID != "")
                {
                    error = Api.AdkUpdate(pData);
                    CheckError("Update Article Number: " + articleNumber, error, tempFolderPath);
                    if (error.lRc == Api.ADKE_OK)
                    {
                        File.AppendAllText(tempFolderPath + "ImportLog.txt", "Artikel är uppdaterad" + Environment.NewLine);
                    }
                    else
                    {
                        productsLeft.Add(product);
                    }
                }
                else
                {
                    error = Api.AdkAdd(pData);
                    CheckError("Add ProductName: " + articleName, error, tempFolderPath);
                    if (error.lRc == Api.ADKE_OK)
                    {
                        File.AppendAllText(tempFolderPath + "ImportLog.txt", "Artikel är skapad" + Environment.NewLine);
                    }
                    else
                    {
                        productsLeft.Add(product);
                    }
                }
                error = Api.AdkDeleteStruct(pData);
                CheckError("ProductName " + articleName, error, tempFolderPath);

                Api.AdkDeleteStruct(pData);
            }
            return productsLeft;
        }

        public static void CheckError(string id, Api.ADKERROR error, string tempFolderPath)
        {
            if (error.lRc != Api.ADKE_OK)
            {
                if (error.lRc != Api.ADKE_OK)
                {
                    String errortext = new String(' ', 200);
                    String errortextTable = new String(' ', 200);
                    String errortextField = new String(' ', 200);
                    int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                    Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                    Console.WriteLine(errortext + " " + id);
                    errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elDbTable;
                    Api.AdkGetErrorText(ref error, errtype, ref errortextTable, 200);
                    Console.WriteLine(errortextTable + " " + id);
                    errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elField;
                    Api.AdkGetErrorText(ref error, errtype, ref errortextField, 200);
                    Console.WriteLine(errortextField + " " + id);
                    File.AppendAllText(tempFolderPath + "ImportLog.txt", "Error: " + errortext + " " + id + Environment.NewLine);
                    File.AppendAllText(tempFolderPath + "ImportLog.txt", "Error tabell: " + errortextTable + Environment.NewLine);
                    File.AppendAllText(tempFolderPath + "ImportLog.txt", "Error fält: " + errortextField + Environment.NewLine);
                }
            }
        }

        public static bool GetUnit(String Enhetskod)
        {
            if (Enhetskod != "")
            { }
            bool exsist = false;
            Api.ADKERROR error;
            int pDataFind = 0;
            string EnhetText = Enhetskod;
            String EnhetskodResult = new String(' ', 4);
            pDataFind = Api.AdkCreateData(Api.ADK_DB_CODE_OF_UNIT);
            error = Api.AdkSetStr(pDataFind, Api.ADK_CODE_OF_UNIT_CODE, ref Enhetskod);
            error = Api.AdkFind(pDataFind);
            Api.AdkGetStr(pDataFind, Api.ADK_CODE_OF_UNIT_CODE, ref EnhetskodResult, 4);
            if (error.lRc != Api.ADKE_OK)
            {
                String errortext = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
            }
            else if (Enhetskod == EnhetskodResult)
            {
                EnhetText = new String(' ', 12);
                error = Api.AdkGetStr(pDataFind, Api.ADK_CODE_OF_UNIT_TEXT, ref EnhetText, 12);
                exsist = true;
            }

            error = Api.AdkDeleteStruct(pDataFind);
            return exsist;
        }

        public static bool GetArticleGroup(String ArticleGroupKod)
        {
            if (ArticleGroupKod != "")
            { }
            bool exsist = false;
            Api.ADKERROR error;
            int pDataFind = 0;
            string ArticleGroupText = "";
            String ArticleGroupkodResult = new String(' ', 6);
            pDataFind = Api.AdkCreateData(Api.ADK_DB_CODE_OF_ARTICLE_GROUP);
            error = Api.AdkSetStr(pDataFind, Api.ADK_CODE_OF_ARTICLE_GROUP_CODE, ref ArticleGroupKod);
            error = Api.AdkFind(pDataFind);
            Api.AdkGetStr(pDataFind, Api.ADK_CODE_OF_ARTICLE_GROUP_CODE, ref ArticleGroupkodResult, 6);
            if (error.lRc != Api.ADKE_OK)
            {
                String errortext = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
            }
            else if (ArticleGroupKod == ArticleGroupkodResult)
            {
                ArticleGroupText = new String(' ', 25);
                error = Api.AdkGetStr(pDataFind, Api.ADK_CODE_OF_ARTICLE_GROUP_TEXT, ref ArticleGroupText, 25);
                exsist = true;
            }

            error = Api.AdkDeleteStruct(pDataFind);
            return exsist;
        }




    }
}
