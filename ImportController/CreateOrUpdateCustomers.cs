﻿using System;
using System.Collections.Generic;
using AdkNet4Wrapper;
using System.IO;

namespace MyGizmoVisma
{
    class CreateOrUpdateCustomers
    {
        private int pData;
        private Api.ADKERROR error;
        public List<Customers> CreateOrUpdate(List<Customers> customers, string tempFolderPath)
        {
            File.AppendAllText(tempFolderPath + "ImportLog.txt", Environment.NewLine + "Import av kunder" + Environment.NewLine);
            File.AppendAllText(tempFolderPath + "ImportLog.txt", "================" + Environment.NewLine);
            List<Customers> customersLeft = new List<Customers>();

            foreach (Customers customer in customers)
            {
                File.AppendAllText(tempFolderPath + "ImportLog.txt", "Kund: " + customer.ErpCustomerID + ", "
                    + customer.Name + Environment.NewLine);

                pData = Api.AdkCreateData(Api.ADK_DB_CUSTOMER);

                string customerNumber = "";
                if (customer.ErpCustomerID != null)
                {
                    customerNumber = customer.ErpCustomerID;
                    error = Api.AdkSetStr(pData, Api.ADK_CUSTOMER_NUMBER, ref customerNumber);
                    CheckError("CustomerNumber " + customerNumber, error, tempFolderPath);
                }

                string CustomerNamne = "";
                if (customer.Name != null)
                {
                    CustomerNamne = customer.Name;
                    error = Api.AdkSetStr(pData, Api.ADK_CUSTOMER_NAME, ref CustomerNamne);
                    CheckError("CustomerNamne: " + CustomerNamne, error, tempFolderPath);
                }

                if (customer.AdressLine1 != null)
                {
                    string BillingStreet = customer.AdressLine1;
                    error = Api.AdkSetStr(pData, Api.ADK_CUSTOMER_MAILING_ADDRESS, ref BillingStreet);
                    CheckError("AdressLine1 CustomerNamne: " + CustomerNamne, error, tempFolderPath);
                }

                if (customer.AdressLine2 != null)
                {
                    string BillingStreet = customer.AdressLine2;
                    error = Api.AdkSetStr(pData, Api.ADK_CUSTOMER_MAILING_ADDRESS2, ref BillingStreet);
                    CheckError("AdressLine2 CustomerNamne: " + CustomerNamne, error, tempFolderPath);
                }

                if (customer.PostalCode != null)
                {
                    string BillingZip = customer.PostalCode;
                    error = Api.AdkSetStr(pData, Api.ADK_CUSTOMER_ZIPCODE, ref BillingZip);
                    CheckError("PostalCode CustomerNamne: " + CustomerNamne, error, tempFolderPath);
                }

                if (customer.City != null)
                {
                    string BillingCity = customer.City;
                    error = Api.AdkSetStr(pData, Api.ADK_CUSTOMER_CITY, ref BillingCity);
                    CheckError("City CustomerNamne: " + CustomerNamne, error, tempFolderPath);
                }

                if (customer.Country != null)
                {
                    string BillingCountry = customer.Country;
                    error = Api.AdkSetStr(pData, Api.ADK_CUSTOMER_COUNTRY, ref BillingCountry);
                    CheckError("Country CustomerNamne: " + CustomerNamne, error, tempFolderPath);
                }

                if (customer.PhoneNumber != null)
                {
                    string BillingPhone = customer.PhoneNumber;
                    error = Api.AdkSetStr(pData, Api.ADK_CUSTOMER_TELEPHONE, ref BillingPhone);
                    CheckError("PhoneNumber CustomerNamne: " + CustomerNamne, error, tempFolderPath);
                }

                if (customer.Email != null)
                {
                    string BillingEmail = customer.Email;
                    error = Api.AdkSetStr(pData, Api.ADK_CUSTOMER_EMAIL, ref BillingEmail);
                    CheckError("Email CustomerNamne: " + CustomerNamne, error, tempFolderPath);
                }

                if (customer.OrganisationNumber != null)
                {
                    string OrganisationNumber = customer.OrganisationNumber;
                    CheckError("OrganisationNumber CustomerNamne: " + CustomerNamne, error, tempFolderPath);
                }

                if (customer.ContactPerson != null)
                {
                    string ContactPerson = customer.ContactPerson;
                    error = Api.AdkSetStr(pData, Api.ADK_CUSTOMER_REFERENCE, ref ContactPerson);
                    CheckError("ContactPerson CustomerNamne: " + CustomerNamne, error, tempFolderPath);
                }                

                if (customer.ErpCustomerID != null && customer.ErpCustomerID != "")
                {
                    error = Api.AdkUpdate(pData);
                    CheckError("Update Customer Number: " + CustomerNamne, error, tempFolderPath);
                    if (error.lRc == Api.ADKE_OK)
                    {
                        File.AppendAllText(tempFolderPath + "ImportLog.txt", "Kunden är uppdaterad" + Environment.NewLine);
                    }
                    else
                    {
                        customersLeft.Add(customer);
                    }
                }
                else
                {
                    error = Api.AdkAdd(pData);
                    CheckError("CustomerNamne: " + CustomerNamne, error, tempFolderPath);
                    if (error.lRc == Api.ADKE_OK)
                    {
                        File.AppendAllText(tempFolderPath + "ImportLog.txt", "Kunden är skapad" + Environment.NewLine);
                    }
                    else
                    {
                        customersLeft.Add(customer);
                    }
                }

                Api.AdkDeleteStruct(pData);
            }
            return customersLeft;
        }


        public static void CheckError(string id, Api.ADKERROR error, string tempFolderPath)
        {
            if (error.lRc != Api.ADKE_OK)
            {
                if (error.lRc != Api.ADKE_OK)
                {
                    String errortext = new String(' ', 200);
                    String errortextTable = new String(' ', 200);
                    String errortextField = new String(' ', 200);
                    int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                    Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                    Console.WriteLine(errortext + " " + id);
                    errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elDbTable;
                    Api.AdkGetErrorText(ref error, errtype, ref errortextTable, 200);
                    Console.WriteLine(errortextTable + " " + id);
                    errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elField;
                    Api.AdkGetErrorText(ref error, errtype, ref errortextField, 200);
                    Console.WriteLine(errortextField + " " + id);
                    File.AppendAllText(tempFolderPath + "ImportLog.txt", "Error: " + errortext + " " + id + Environment.NewLine);
                    File.AppendAllText(tempFolderPath + "ImportLog.txt", "Error tabell: " + errortextTable + Environment.NewLine);
                    File.AppendAllText(tempFolderPath + "ImportLog.txt", "Error fält: " + errortextField + Environment.NewLine);
                }
            }
        }
    }
}
