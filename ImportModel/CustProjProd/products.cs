﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyGizmoVisma
{
    class Products
    {
        public string id { get; set; }
        public string ProductName { get; set; }
        public string ProductKind { get; set; }
        public string ProductTypeID { get; set; }
        public string ProductTypeName { get; set; }
        public string UnitID { get; set; }
        public string UnitName { get; set; }
        public string Checkbox { get; set; }
        public string Price { get; set; }
        public string Active { get; set; }
        public string ChangeDate { get; set; }
        public string ErpProductID { get; set; }
    }
}
