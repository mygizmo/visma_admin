﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyGizmoVisma
{
    class Customers
    {       
        public string id { get; set; }        
        public string Name { get; set; }       
        public string OrganisationNumber { get; set; }
        public string Email { get; set; }
        public string PhoneNumber { get; set; }
        public string AdressLine1 { get; set; }
        public string AdressLine2 { get; set; }
        public string PostalCode { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string ContactPerson { get; set; }   
        public string Active { get; set; }
        public string ChangeDate { get; set; }
        public string ErpCustomerID { get; set; }
    }
}
