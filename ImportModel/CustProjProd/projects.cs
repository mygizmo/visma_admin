﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyGizmoVisma
{
    class Projects
    {
        public string id { get; set; }
        public string ProjectName { get; set; }
        public string Description { get; set; }
        public string CustomerID { get; set; }
        public string CustomerName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string Active { get; set; }
        public string ChangeDate { get; set; }
    }
}
