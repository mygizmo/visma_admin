﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyGizmoVisma
{
    class CustProjProd
    {
        [JsonProperty(PropertyName = "documentCreateDate")]
        public string documentCreateDate { get; set; }

        [JsonProperty(PropertyName = "customers")]
        public List<Customers> Customers { get; set; }
        
        [JsonProperty(PropertyName = "projects")]
        public List<Projects> Projects { get; set; }

        [JsonProperty(PropertyName = "products")]
        public List<Products> Products { get; set; }

        [JsonProperty(PropertyName = "invoices")]
        public List<invoices> invoices { get; set; }


    }
}
