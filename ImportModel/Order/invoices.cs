﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyGizmoVisma
{
    class invoices
    {
        [JsonProperty(PropertyName = "invoiceHead")]        
        public invoiceHead invoiceHead { get; set; }

        [JsonProperty(PropertyName = "invoiceLines")]
        public invoiceLines invoiceLines { get; set; }

    }
}
