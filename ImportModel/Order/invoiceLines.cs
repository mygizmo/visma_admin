﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyGizmoVisma
{
    public class invoiceLines
    {       
        [JsonProperty(PropertyName = "products")]
        public IList<ILproducts> products { get; set; }

        [JsonProperty(PropertyName = "notes")]
        public IList<ILnotes> notes { get; set; }

    }
}
