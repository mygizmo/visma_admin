﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyGizmoVisma
{
    public class invoiceHead
    {
        public string InvoiceID { get; set; }
        public string CustomerName { get; set; }
        public DateTime Date { get; set; }
        public string ErpCustomerID { get; set; }
        public string ProjectName { get; set; }
        public string ProjectDescription { get; set; }
        public string ErpProjectID { get; set; }
        public string ErpOrderNumber { get; set; }

    }
}
