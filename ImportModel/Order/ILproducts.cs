﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGizmoVisma
{
    public class ILproducts
    {
        public DateTime Date { get; set; }
        public string EmployeeFirstName { get; set; }
        public string EmployeeLastName { get; set; }
        public string ErpProductID { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string Quantity { get; set; }
        public string Price { get; set; }
        public string UnitName { get; set; }
        public string TextRow { get; set; }


    }
}
