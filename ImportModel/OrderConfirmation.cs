﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MyGizmoVisma
{
    class OrderConfirmation
    {
        
        [JsonProperty(PropertyName = "Visma Order Id")]
        public string VismaOrderId { get; set; }

        [JsonProperty(PropertyName = "Import Datum")]
        public string ImportDate { get; set; }

        [JsonProperty(PropertyName = "Operation")]
        public string Mode { get; set; }


    }
}
