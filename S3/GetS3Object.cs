﻿using System;
using System.Collections.Generic;
using System.Configuration;

using MyGizmoVisma.Signers;
using MyGizmoVisma.Util;

namespace MyGizmoVisma
{
    
    public class GetS3Object
    {
        static readonly string AWSAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
        static readonly string AWSSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];        

        /// <summary>
        /// Request the content of an object from the specified bucket using virtual hosted-style 
        /// object addressing.
        /// </summary>
        
        public string Run()
        {
            Console.WriteLine("GetS3Object");
            string reply = "";
            
            var regionUrlPart = string.Empty; 
            var endpointUri = string.Format(ConfigurationManager.AppSettings["endpointUri"] + "exportFromMG.json");

            var uri = new Uri(endpointUri);

            // for a simple GET, we have no body so supply the precomputed 'empty' hash
            var headers = new Dictionary<string, string>
            {
                {AWS4SignerBase.X_Amz_Content_SHA256, AWS4SignerBase.EMPTY_BODY_SHA256},
                {"content-type", "text/plain"}
            };

            var signer = new AWS4SignerForAuthorizationHeader
            {
                EndpointUri = uri,
                HttpMethod = "GET",
                Service = "s3",
                Region = "eu-central-1"
            };

            var authorization = signer.ComputeSignature(headers,
                                                        "",   
                                                        AWS4SignerBase.EMPTY_BODY_SHA256,
                                                        AWSAccessKey,
                                                        AWSSecretKey);

            // place the computed signature into a formatted 'Authorization' header 
            // and call S3
            headers.Add("Authorization", authorization);

            reply = HttpHelpers.InvokeHttpRequest(uri, "GET", headers, null);
            return reply;
        }
    }
}
