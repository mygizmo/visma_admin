﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Text;

using MyGizmoVisma.Signers;
using MyGizmoVisma.Util;

namespace MyGizmoVisma
{
    
    public class PutS3Object
    {
        static readonly string AWSAccessKey = ConfigurationManager.AppSettings["AWSAccessKey"];
        static readonly string AWSSecretKey = ConfigurationManager.AppSettings["AWSSecretKey"];
        
        /// <summary>
        /// Uploads content to an Amazon S3 object in a single call using Signature V4 authorization.
        /// </summary>
        public void Run(string ObjectName, string ObjectContent)// string region, string bucketName, string objectKey
        {              
            var endpointUri = string.Format(ConfigurationManager.AppSettings["endpointUri"] + ObjectName + ".json");

            var uri = new Uri(endpointUri);

            // precompute hash of the body content
            var contentHash = AWS4SignerBase.CanonicalRequestHashAlgorithm.ComputeHash(Encoding.UTF8.GetBytes(ObjectContent));
            var contentHashString = AWS4SignerBase.ToHexString(contentHash, true);

            var headers = new Dictionary<string, string>
            {
                {AWS4SignerBase.X_Amz_Content_SHA256, contentHashString}, 
                {"content-type", "text/plain"}
            };

            var signer = new AWS4SignerForAuthorizationHeader
            {
                EndpointUri = uri,
                HttpMethod = "PUT",
                Service = "s3",
                Region = "eu-central-1"
            };

            var authorization = signer.ComputeSignature(headers,
                                                        "",   // no query parameters
                                                        contentHashString,
                                                        AWSAccessKey,
                                                        AWSSecretKey);

            // express authorization for this as a header
            headers.Add("Authorization", authorization);

            // make the call to Amazon S3
            HttpHelpers.InvokeHttpRequest(uri, "PUT", headers, ObjectContent);
        }
    }
}
