using System;

namespace MyGizmoVisma
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.MainMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.avslutaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.administrationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenFtg = new System.Windows.Forms.ToolStripMenuItem();
            this.CloseFtg = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.s = new System.Windows.Forms.TabPage();
            this.groupBoxExportera = new System.Windows.Forms.GroupBox();
            this.checkBoxIncludeInvoiced = new System.Windows.Forms.CheckBox();
            this.checkBoxIncludeCancelled = new System.Windows.Forms.CheckBox();
            this.label1 = new System.Windows.Forms.Label();
            this.datePickerExportFromDate = new System.Windows.Forms.DateTimePicker();
            this.buttonExportera = new System.Windows.Forms.Button();
            this.checkBoxExportArticles = new System.Windows.Forms.CheckBox();
            this.checkBoxExportCustomers = new System.Windows.Forms.CheckBox();
            this.checkBoxExportProjects = new System.Windows.Forms.CheckBox();
            this.checkBoxExportOrders = new System.Windows.Forms.CheckBox();
            this.groupBoxImportera = new System.Windows.Forms.GroupBox();
            this.checkBoxUpdateYourRefWithProjectName = new System.Windows.Forms.CheckBox();
            this.checkBoxUpdateEvenIfDelivered = new System.Windows.Forms.CheckBox();
            this.checkBoxUpdateEvenIfDocumentPrinted = new System.Windows.Forms.CheckBox();
            this.buttonDisplayInfo = new System.Windows.Forms.Button();
            this.checkBoxImportCustomers = new System.Windows.Forms.CheckBox();
            this.checkBoxImportArticles = new System.Windows.Forms.CheckBox();
            this.checkBoxImportInvoices = new System.Windows.Forms.CheckBox();
            this.buttonImportera = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.checkBoxCopyProjectDescriptionAsText = new System.Windows.Forms.CheckBox();
            this.MainMenu.SuspendLayout();
            this.s.SuspendLayout();
            this.groupBoxExportera.SuspendLayout();
            this.groupBoxImportera.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.SuspendLayout();
            // 
            // MainMenu
            // 
            this.MainMenu.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.MainMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.administrationToolStripMenuItem});
            this.MainMenu.Location = new System.Drawing.Point(0, 0);
            this.MainMenu.Name = "MainMenu";
            this.MainMenu.Padding = new System.Windows.Forms.Padding(8, 2, 0, 2);
            this.MainMenu.Size = new System.Drawing.Size(909, 28);
            this.MainMenu.TabIndex = 1;
            this.MainMenu.Text = "Meny";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.avslutaToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(46, 24);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // avslutaToolStripMenuItem
            // 
            this.avslutaToolStripMenuItem.Name = "avslutaToolStripMenuItem";
            this.avslutaToolStripMenuItem.Size = new System.Drawing.Size(140, 26);
            this.avslutaToolStripMenuItem.Text = "A&vsluta";
            this.avslutaToolStripMenuItem.Click += new System.EventHandler(this.avslutaToolStripMenuItem_Click);
            // 
            // administrationToolStripMenuItem
            // 
            this.administrationToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.OpenFtg,
            this.CloseFtg,
            this.toolStripSeparator1});
            this.administrationToolStripMenuItem.Name = "administrationToolStripMenuItem";
            this.administrationToolStripMenuItem.Size = new System.Drawing.Size(121, 24);
            this.administrationToolStripMenuItem.Text = "&Administration";
            // 
            // OpenFtg
            // 
            this.OpenFtg.Name = "OpenFtg";
            this.OpenFtg.Size = new System.Drawing.Size(190, 26);
            this.OpenFtg.Text = "&�ppna f�retag";
            this.OpenFtg.Click += new System.EventHandler(this.OpenFtg_Click);
            // 
            // CloseFtg
            // 
            this.CloseFtg.Name = "CloseFtg";
            this.CloseFtg.Size = new System.Drawing.Size(190, 26);
            this.CloseFtg.Text = "&St�ng f�retag";
            this.CloseFtg.Click += new System.EventHandler(this.CloseFtg_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(187, 6);
            // 
            // s
            // 
            this.s.Controls.Add(this.groupBoxExportera);
            this.s.Controls.Add(this.groupBoxImportera);
            this.s.Location = new System.Drawing.Point(4, 25);
            this.s.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.s.Name = "s";
            this.s.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.s.Size = new System.Drawing.Size(845, 378);
            this.s.TabIndex = 1;
            this.s.Text = "Exportera / Importera";
            this.s.UseVisualStyleBackColor = true;
            // 
            // groupBoxExportera
            // 
            this.groupBoxExportera.Controls.Add(this.checkBoxIncludeInvoiced);
            this.groupBoxExportera.Controls.Add(this.checkBoxIncludeCancelled);
            this.groupBoxExportera.Controls.Add(this.label1);
            this.groupBoxExportera.Controls.Add(this.datePickerExportFromDate);
            this.groupBoxExportera.Controls.Add(this.buttonExportera);
            this.groupBoxExportera.Controls.Add(this.checkBoxExportArticles);
            this.groupBoxExportera.Controls.Add(this.checkBoxExportCustomers);
            this.groupBoxExportera.Controls.Add(this.checkBoxExportProjects);
            this.groupBoxExportera.Controls.Add(this.checkBoxExportOrders);
            this.groupBoxExportera.Location = new System.Drawing.Point(24, 26);
            this.groupBoxExportera.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxExportera.Name = "groupBoxExportera";
            this.groupBoxExportera.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxExportera.Size = new System.Drawing.Size(369, 327);
            this.groupBoxExportera.TabIndex = 6;
            this.groupBoxExportera.TabStop = false;
            this.groupBoxExportera.Text = "Exportera till MyGizmo";
            // 
            // checkBoxIncludeInvoiced
            // 
            this.checkBoxIncludeInvoiced.AutoSize = true;
            this.checkBoxIncludeInvoiced.Enabled = false;
            this.checkBoxIncludeInvoiced.Location = new System.Drawing.Point(60, 242);
            this.checkBoxIncludeInvoiced.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxIncludeInvoiced.Name = "checkBoxIncludeInvoiced";
            this.checkBoxIncludeInvoiced.Size = new System.Drawing.Size(208, 21);
            this.checkBoxIncludeInvoiced.TabIndex = 8;
            this.checkBoxIncludeInvoiced.Text = "Inkludera fakturerade ordrar";
            this.toolTip1.SetToolTip(this.checkBoxIncludeInvoiced, resources.GetString("checkBoxIncludeInvoiced.ToolTip"));
            this.checkBoxIncludeInvoiced.UseVisualStyleBackColor = true;
            // 
            // checkBoxIncludeCancelled
            // 
            this.checkBoxIncludeCancelled.AutoSize = true;
            this.checkBoxIncludeCancelled.Enabled = false;
            this.checkBoxIncludeCancelled.Location = new System.Drawing.Point(60, 218);
            this.checkBoxIncludeCancelled.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxIncludeCancelled.Name = "checkBoxIncludeCancelled";
            this.checkBoxIncludeCancelled.Size = new System.Drawing.Size(209, 21);
            this.checkBoxIncludeCancelled.TabIndex = 7;
            this.checkBoxIncludeCancelled.Text = "Inkludera makulerade ordrar";
            this.toolTip1.SetToolTip(this.checkBoxIncludeCancelled, resources.GetString("checkBoxIncludeCancelled.ToolTip"));
            this.checkBoxIncludeCancelled.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 34);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(300, 17);
            this.label1.TabIndex = 6;
            this.label1.Text = "Exportera poster �ndrade/skapade fr�n datum";
            // 
            // datePickerExportFromDate
            // 
            this.datePickerExportFromDate.CustomFormat = "yyyy-mm-dd";
            this.datePickerExportFromDate.Location = new System.Drawing.Point(9, 60);
            this.datePickerExportFromDate.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.datePickerExportFromDate.Name = "datePickerExportFromDate";
            this.datePickerExportFromDate.ShowCheckBox = true;
            this.datePickerExportFromDate.Size = new System.Drawing.Size(265, 22);
            this.datePickerExportFromDate.TabIndex = 1;
            this.toolTip1.SetToolTip(this.datePickerExportFromDate, "Denna m�ste bockas ur om man inte vill v�lja att exportera\r\nVisma poster p� �ndra" +
        "/skapa datum.");
            // 
            // buttonExportera
            // 
            this.buttonExportera.Location = new System.Drawing.Point(12, 282);
            this.buttonExportera.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonExportera.Name = "buttonExportera";
            this.buttonExportera.Size = new System.Drawing.Size(181, 28);
            this.buttonExportera.TabIndex = 8;
            this.buttonExportera.Text = "Exportera till exportfil";
            this.buttonExportera.UseVisualStyleBackColor = true;
            this.buttonExportera.Click += new System.EventHandler(this.buttonExportera_Click);
            // 
            // checkBoxExportArticles
            // 
            this.checkBoxExportArticles.Location = new System.Drawing.Point(12, 101);
            this.checkBoxExportArticles.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxExportArticles.Name = "checkBoxExportArticles";
            this.checkBoxExportArticles.Size = new System.Drawing.Size(200, 28);
            this.checkBoxExportArticles.TabIndex = 2;
            this.checkBoxExportArticles.Text = "Exportera Artiklar";
            this.toolTip1.SetToolTip(this.checkBoxExportArticles, resources.GetString("checkBoxExportArticles.ToolTip"));
            this.checkBoxExportArticles.UseVisualStyleBackColor = true;
            // 
            // checkBoxExportCustomers
            // 
            this.checkBoxExportCustomers.Location = new System.Drawing.Point(12, 130);
            this.checkBoxExportCustomers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxExportCustomers.Name = "checkBoxExportCustomers";
            this.checkBoxExportCustomers.Size = new System.Drawing.Size(200, 28);
            this.checkBoxExportCustomers.TabIndex = 3;
            this.checkBoxExportCustomers.Text = "Exportera Kunder";
            this.toolTip1.SetToolTip(this.checkBoxExportCustomers, resources.GetString("checkBoxExportCustomers.ToolTip"));
            this.checkBoxExportCustomers.UseVisualStyleBackColor = true;
            // 
            // checkBoxExportProjects
            // 
            this.checkBoxExportProjects.Location = new System.Drawing.Point(12, 161);
            this.checkBoxExportProjects.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxExportProjects.Name = "checkBoxExportProjects";
            this.checkBoxExportProjects.Size = new System.Drawing.Size(200, 28);
            this.checkBoxExportProjects.TabIndex = 4;
            this.checkBoxExportProjects.Text = "Exportera Projekt";
            this.checkBoxExportProjects.UseVisualStyleBackColor = true;
            // 
            // checkBoxExportOrders
            // 
            this.checkBoxExportOrders.Location = new System.Drawing.Point(12, 192);
            this.checkBoxExportOrders.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxExportOrders.Name = "checkBoxExportOrders";
            this.checkBoxExportOrders.Size = new System.Drawing.Size(200, 28);
            this.checkBoxExportOrders.TabIndex = 5;
            this.checkBoxExportOrders.Text = "Exportera Ordrar";
            this.toolTip1.SetToolTip(this.checkBoxExportOrders, resources.GetString("checkBoxExportOrders.ToolTip"));
            this.checkBoxExportOrders.UseVisualStyleBackColor = true;
            this.checkBoxExportOrders.CheckedChanged += new System.EventHandler(this.checkBoxExportOrders_CheckedChanged);
            // 
            // groupBoxImportera
            // 
            this.groupBoxImportera.Controls.Add(this.checkBoxCopyProjectDescriptionAsText);
            this.groupBoxImportera.Controls.Add(this.checkBoxUpdateYourRefWithProjectName);
            this.groupBoxImportera.Controls.Add(this.checkBoxUpdateEvenIfDelivered);
            this.groupBoxImportera.Controls.Add(this.checkBoxUpdateEvenIfDocumentPrinted);
            this.groupBoxImportera.Controls.Add(this.buttonDisplayInfo);
            this.groupBoxImportera.Controls.Add(this.checkBoxImportCustomers);
            this.groupBoxImportera.Controls.Add(this.checkBoxImportArticles);
            this.groupBoxImportera.Controls.Add(this.checkBoxImportInvoices);
            this.groupBoxImportera.Controls.Add(this.buttonImportera);
            this.groupBoxImportera.Location = new System.Drawing.Point(401, 26);
            this.groupBoxImportera.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxImportera.Name = "groupBoxImportera";
            this.groupBoxImportera.Padding = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.groupBoxImportera.Size = new System.Drawing.Size(417, 327);
            this.groupBoxImportera.TabIndex = 7;
            this.groupBoxImportera.TabStop = false;
            this.groupBoxImportera.Text = "Importera fr�n MyGizmo";
            // 
            // checkBoxCopyProjectDescriptionAsText
            // 
            this.checkBoxCopyProjectDescriptionAsText.AutoSize = true;
            this.checkBoxCopyProjectDescriptionAsText.Enabled = false;
            this.checkBoxCopyProjectDescriptionAsText.Location = new System.Drawing.Point(63, 192);
            this.checkBoxCopyProjectDescriptionAsText.Margin = new System.Windows.Forms.Padding(4);
            this.checkBoxCopyProjectDescriptionAsText.Name = "checkBoxCopyProjectDescriptionAsText";
            this.checkBoxCopyProjectDescriptionAsText.Size = new System.Drawing.Size(436, 26);
            this.checkBoxCopyProjectDescriptionAsText.TabIndex = 9;
            this.checkBoxCopyProjectDescriptionAsText.Text = "Kopiera in projektbeskrivning som text p� underlag";
            this.toolTip1.SetToolTip(this.checkBoxCopyProjectDescriptionAsText, "Om denna bockas i, s� kommer projektes beskrivningen att kporieras i som text p�" +
        " fakturaunderlaget. ");
            this.checkBoxCopyProjectDescriptionAsText.UseVisualStyleBackColor = true;
            // 
            // checkBoxUpdateYourRefWithProjectName
            // 
            this.checkBoxUpdateYourRefWithProjectName.AutoSize = true;
            this.checkBoxUpdateYourRefWithProjectName.Enabled = false;
            this.checkBoxUpdateYourRefWithProjectName.Location = new System.Drawing.Point(63, 169);
            this.checkBoxUpdateYourRefWithProjectName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxUpdateYourRefWithProjectName.Name = "checkBoxUpdateYourRefWithProjectName";
            this.checkBoxUpdateYourRefWithProjectName.Size = new System.Drawing.Size(250, 21);
            this.checkBoxUpdateYourRefWithProjectName.TabIndex = 8;
            this.checkBoxUpdateYourRefWithProjectName.Text = "Uppdatera Er ref med Projektnamn";
            this.toolTip1.SetToolTip(this.checkBoxUpdateYourRefWithProjectName, "Om denna bockas i, s� kommer Er referense p� ordern att uppdateras med Projektnam" +
        "net fr�n MyGizmo. ");
            this.checkBoxUpdateYourRefWithProjectName.UseVisualStyleBackColor = true;
            // 
            // checkBoxUpdateEvenIfDelivered
            // 
            this.checkBoxUpdateEvenIfDelivered.AutoSize = true;
            this.checkBoxUpdateEvenIfDelivered.Enabled = false;
            this.checkBoxUpdateEvenIfDelivered.Location = new System.Drawing.Point(63, 145);
            this.checkBoxUpdateEvenIfDelivered.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxUpdateEvenIfDelivered.Name = "checkBoxUpdateEvenIfDelivered";
            this.checkBoxUpdateEvenIfDelivered.Size = new System.Drawing.Size(219, 21);
            this.checkBoxUpdateEvenIfDelivered.TabIndex = 6;
            this.checkBoxUpdateEvenIfDelivered.Text = "Uppdatera �ven om levererad";
            this.toolTip1.SetToolTip(this.checkBoxUpdateEvenIfDelivered, "Om denna bockas i, s� kommer �ven ordrar som redan �r levererade \r\natt uppdateras" +
        " med nya orderrader. ");
            this.checkBoxUpdateEvenIfDelivered.UseVisualStyleBackColor = true;
            // 
            // checkBoxUpdateEvenIfDocumentPrinted
            // 
            this.checkBoxUpdateEvenIfDocumentPrinted.AutoSize = true;
            this.checkBoxUpdateEvenIfDocumentPrinted.Enabled = false;
            this.checkBoxUpdateEvenIfDocumentPrinted.Location = new System.Drawing.Point(63, 121);
            this.checkBoxUpdateEvenIfDocumentPrinted.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxUpdateEvenIfDocumentPrinted.Name = "checkBoxUpdateEvenIfDocumentPrinted";
            this.checkBoxUpdateEvenIfDocumentPrinted.Size = new System.Drawing.Size(347, 21);
            this.checkBoxUpdateEvenIfDocumentPrinted.TabIndex = 7;
            this.checkBoxUpdateEvenIfDocumentPrinted.Text = "Uppdatera �ven om ordererk�nnande �r utskriven";
            this.toolTip1.SetToolTip(this.checkBoxUpdateEvenIfDocumentPrinted, "Om denna bockas i, s� kommer �ven odrar d�r ordererk�nnande redan �r utskriven \r\n" +
        "att uppdateras med nya orderrader. ");
            this.checkBoxUpdateEvenIfDocumentPrinted.UseVisualStyleBackColor = true;
            // 
            // buttonDisplayInfo
            // 
            this.buttonDisplayInfo.Location = new System.Drawing.Point(227, 247);
            this.buttonDisplayInfo.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonDisplayInfo.Name = "buttonDisplayInfo";
            this.buttonDisplayInfo.Size = new System.Drawing.Size(135, 28);
            this.buttonDisplayInfo.TabIndex = 7;
            this.buttonDisplayInfo.Text = "Visa importlogg";
            this.buttonDisplayInfo.UseVisualStyleBackColor = true;
            this.buttonDisplayInfo.Click += new System.EventHandler(this.buttonDisplayInfo_Click);
            // 
            // checkBoxImportCustomers
            // 
            this.checkBoxImportCustomers.AutoSize = true;
            this.checkBoxImportCustomers.Location = new System.Drawing.Point(12, 68);
            this.checkBoxImportCustomers.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxImportCustomers.Name = "checkBoxImportCustomers";
            this.checkBoxImportCustomers.Size = new System.Drawing.Size(140, 21);
            this.checkBoxImportCustomers.TabIndex = 2;
            this.checkBoxImportCustomers.Text = "Importera Kunder";
            this.toolTip1.SetToolTip(this.checkBoxImportCustomers, "Om denna bockas i, s� kommer kunder fr�n MyGizmo att importeras som kunder i Vism" +
        "a. ");
            this.checkBoxImportCustomers.UseVisualStyleBackColor = true;
            // 
            // checkBoxImportArticles
            // 
            this.checkBoxImportArticles.AutoSize = true;
            this.checkBoxImportArticles.Location = new System.Drawing.Point(12, 39);
            this.checkBoxImportArticles.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxImportArticles.Name = "checkBoxImportArticles";
            this.checkBoxImportArticles.Size = new System.Drawing.Size(138, 21);
            this.checkBoxImportArticles.TabIndex = 1;
            this.checkBoxImportArticles.Text = "Importera Artiklar";
            this.toolTip1.SetToolTip(this.checkBoxImportArticles, "Om denna bockas i, s� kommer produkter fr�n MyGizmo att importeras som artiklar i" +
        " Visma. ");
            this.checkBoxImportArticles.UseVisualStyleBackColor = true;
            // 
            // checkBoxImportInvoices
            // 
            this.checkBoxImportInvoices.AutoSize = true;
            this.checkBoxImportInvoices.Location = new System.Drawing.Point(12, 97);
            this.checkBoxImportInvoices.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.checkBoxImportInvoices.Name = "checkBoxImportInvoices";
            this.checkBoxImportInvoices.Size = new System.Drawing.Size(198, 21);
            this.checkBoxImportInvoices.TabIndex = 3;
            this.checkBoxImportInvoices.Text = "Importera Fakturaunderlag";
            this.toolTip1.SetToolTip(this.checkBoxImportInvoices, "Om denna bockas i, s� kommer fakturaunderlag fr�n MyGizmo att importeras som ord" +
        "ar i Visma. ");
            this.checkBoxImportInvoices.UseVisualStyleBackColor = true;
            this.checkBoxImportInvoices.CheckedChanged += new System.EventHandler(this.checkBoxImportInvoices_CheckedChanged);
            // 
            // buttonImportera
            // 
            this.buttonImportera.Location = new System.Drawing.Point(12, 247);
            this.buttonImportera.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.buttonImportera.Name = "buttonImportera";
            this.buttonImportera.Size = new System.Drawing.Size(181, 28);
            this.buttonImportera.TabIndex = 6;
            this.buttonImportera.Text = "Importera fr�n importfil";
            this.buttonImportera.UseVisualStyleBackColor = true;
            this.buttonImportera.Click += new System.EventHandler(this.buttonImportera_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.s);
            this.tabControl1.Location = new System.Drawing.Point(16, 33);
            this.tabControl1.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(853, 407);
            this.tabControl1.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 447);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.MainMenu);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.MainMenu;
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(927, 494);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(927, 494);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MyGizmoVisma";
            this.MainMenu.ResumeLayout(false);
            this.MainMenu.PerformLayout();
            this.s.ResumeLayout(false);
            this.groupBoxExportera.ResumeLayout(false);
            this.groupBoxExportera.PerformLayout();
            this.groupBoxImportera.ResumeLayout(false);
            this.groupBoxImportera.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.MenuStrip MainMenu;
        private System.Windows.Forms.ToolStripMenuItem administrationToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem OpenFtg;
        private System.Windows.Forms.ToolStripMenuItem CloseFtg;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem avslutaToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.TabPage s;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.CheckBox checkBoxExportArticles;
        private System.Windows.Forms.CheckBox checkBoxExportCustomers;
        private System.Windows.Forms.CheckBox checkBoxExportProjects;
        private System.Windows.Forms.CheckBox checkBoxExportOrders;
        private System.Windows.Forms.Button buttonExportera;
        private System.Windows.Forms.Button buttonImportera;
        private System.Windows.Forms.GroupBox groupBoxExportera;
        private System.Windows.Forms.GroupBox groupBoxImportera;
        private System.Windows.Forms.CheckBox checkBoxImportCustomers;
        private System.Windows.Forms.CheckBox checkBoxImportArticles;
        private System.Windows.Forms.CheckBox checkBoxImportInvoices;
        private System.Windows.Forms.DateTimePicker datePickerExportFromDate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button buttonDisplayInfo;
        private System.Windows.Forms.CheckBox checkBoxIncludeCancelled;
        private System.Windows.Forms.CheckBox checkBoxIncludeInvoiced;
        private System.Windows.Forms.CheckBox checkBoxUpdateEvenIfDelivered;
        private System.Windows.Forms.CheckBox checkBoxUpdateEvenIfDocumentPrinted;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.CheckBox checkBoxUpdateYourRefWithProjectName;
        private System.Windows.Forms.CheckBox checkBoxCopyProjectDescriptionAsText;
    }
}

