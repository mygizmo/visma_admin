namespace MyGizmoVisma
{
    partial class PathSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.PathsFtg = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.PathsSys = new System.Windows.Forms.TextBox();
            this.PATH_BN_OPEN = new System.Windows.Forms.Button();
            this.PATH_CANCEL = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(43, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "F�retag";
            // 
            // PathsFtg
            // 
            this.PathsFtg.Location = new System.Drawing.Point(13, 30);
            this.PathsFtg.Name = "PathsFtg";
            this.PathsFtg.Size = new System.Drawing.Size(508, 20);
            this.PathsFtg.TabIndex = 1;
            this.PathsFtg.Text = pftg;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 53);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(93, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Gemensamma filer";
            // 
            // PathsSys
            // 
            this.PathsSys.Location = new System.Drawing.Point(13, 70);
            this.PathsSys.Name = "PathsSys";
            this.PathsSys.Size = new System.Drawing.Size(508, 20);
            this.PathsSys.TabIndex = 3;
            this.PathsSys.Text = psys;
            // 
            // PATH_BN_OPEN
            // 
            this.PATH_BN_OPEN.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.PATH_BN_OPEN.Location = new System.Drawing.Point(446, 99);
            this.PATH_BN_OPEN.Name = "PATH_BN_OPEN";
            this.PATH_BN_OPEN.Size = new System.Drawing.Size(75, 23);
            this.PATH_BN_OPEN.TabIndex = 4;
            this.PATH_BN_OPEN.Text = "�ppna";
            this.PATH_BN_OPEN.UseVisualStyleBackColor = true;
            this.PATH_BN_OPEN.Click += new System.EventHandler(this.PATH_BN_OPEN_Click);
            // 
            // PATH_CANCEL
            // 
            this.PATH_CANCEL.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.PATH_CANCEL.Location = new System.Drawing.Point(365, 99);
            this.PATH_CANCEL.Name = "PATH_CANCEL";
            this.PATH_CANCEL.Size = new System.Drawing.Size(75, 23);
            this.PATH_CANCEL.TabIndex = 5;
            this.PATH_CANCEL.Text = "Avbryt";
            this.PATH_CANCEL.UseVisualStyleBackColor = true;
            // 
            // PathSettings
            // 
            this.AcceptButton = this.PATH_BN_OPEN;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.PATH_CANCEL;
            this.ClientSize = new System.Drawing.Size(535, 136);
            this.ControlBox = false;
            this.Controls.Add(this.PATH_CANCEL);
            this.Controls.Add(this.PATH_BN_OPEN);
            this.Controls.Add(this.PathsSys);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.PathsFtg);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximumSize = new System.Drawing.Size(541, 168);
            this.MinimumSize = new System.Drawing.Size(541, 168);
            this.Name = "PathSettings";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "PathSettings";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PathsFtg;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox PathsSys;
        private System.Windows.Forms.Button PATH_BN_OPEN;
        private System.Windows.Forms.Button PATH_CANCEL;
    }
}