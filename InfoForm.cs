﻿using System;
using System.IO;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MyGizmoVisma
{
    public partial class InfoForm : Form
    {
        public InfoForm()
        {
            InitializeComponent();
            string tempFolderPath = ConfigurationManager.AppSettings["TempFolderPath"];
            string importLogText = File.ReadAllText(tempFolderPath + "ImportLog.txt");
            this.infoTextBox.Text = importLogText;
        }
    }
}
