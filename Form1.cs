using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AdkNet4Wrapper;
using System.IO;
using Newtonsoft.Json;
using System.Configuration;
using System.Reflection;
using System.Net;
using System.Linq;

namespace MyGizmoVisma
{
    public partial class MainForm : Form
    {
        private int pData;
        private int pData2;
        private Api.ADKERROR error;
        private string adminVariant;
        

        public MainForm()
        {
            InitializeComponent();
            pData = 0;
            pData2 = 0;
            ChangeEnable(false);
            adminVariant = "";
        }

        private void ChangeEnable(bool enable)
        {

        }

        private void OpenFtg_Click(object sender, EventArgs e)
        {
            PathSettings ps = new PathSettings();

            if (ps.ShowDialog(this) != DialogResult.Cancel)
            {
                String SysPath = ps.psys;
                String SysFtg = ps.pftg;

                ps.Dispose();

                Api.ADKERROR error = Api.AdkOpen(ref SysPath, ref SysFtg);
                if (error.lRc != 0)
                {
                    String errortext = new String(' ', 200);
                    int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                    Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                    MessageBox.Show(errortext);
                    pData = 0;
                }
                else
                {
                    int variant = 0;
                    int api = 0;
                    String buf = new String(' ', 50);
                    error = Api.AdkGetVariant(ref variant, ref api, ref buf, 50, ref SysPath);
                    if (error.lRc == 0)
                    {
                        adminVariant = buf;
                    }

                    pData = Api.AdkCreateData(Api.ADK_DB_CUSTOMER);
                    pData2 = Api.AdkCreateData(Api.ADK_DB_INVOICE_HEAD);
                    ChangeEnable(true);
                }
            }
        }

        private void CloseFtg_Click(object sender, EventArgs e)
        {
            Api.AdkClose();
            ChangeEnable(false);
        }

        private void avslutaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (pData != 0)
            {
                error = Api.AdkDeleteStruct(pData);
                if (error.lRc != Api.ADKE_OK)
                {
                    String errortext = new String(' ', 200);
                    int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                    Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                    MessageBox.Show(errortext);
                }
            }

            if (pData2 != 0)
            {
                error = Api.AdkDeleteStruct(pData2);
                if (error.lRc != Api.ADKE_OK)
                {
                    String errortext = new String(' ', 200);
                    int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                    Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                    MessageBox.Show(errortext);
                }
            }

            Api.AdkClose();

            this.Dispose();
            this.Close();
        }

        private void buttonImportera_Click(object sender, EventArgs e)
        {
            string resultatOutput = "";
            string tempFolderPath = ConfigurationManager.AppSettings["TempFolderPath"];
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                GetS3Object getS3Object = new GetS3Object();
                string stringgetS3Reply = "";
                stringgetS3Reply = getS3Object.Run();

                // Get Visma integration version
                int major = 0;
                int minor = 0;
                Api.AdkGetVersion(ref major, ref minor);

                Console.WriteLine("Ansluter mot Visma");
                File.WriteAllText(tempFolderPath + "ImportLog.txt", String.Empty);
                File.AppendAllText(tempFolderPath + "Importlog.txt", "Importlogg: " + DateTime.Now + Environment.NewLine);
                File.AppendAllText(tempFolderPath + "Importlog.txt", "Visma integrationsversion: " + major + "." + minor 
                    + Environment.NewLine);

                int lineNumber = (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber();

                string sysPat = ConfigurationManager.AppSettings["SysPat"];
                string sysFtg = ConfigurationManager.AppSettings["SysFtg"];                

                Api.ADKERROR error = Api.AdkOpen(ref sysPat, ref sysFtg);
                CheckError("�ppna", error, "import");

                object deserializeObject = JsonConvert.DeserializeObject<object>(stringgetS3Reply);
                CustProjProd custProjProd = new CustProjProd(); //deserializeObject
                string jsonString = Convert.ToString(deserializeObject);
                CustProjProd deserializedOrder = JsonConvert.DeserializeObject<CustProjProd>(jsonString);
                custProjProd = deserializedOrder;

                // Invoices
                if (this.checkBoxImportInvoices.Checked)
                {
                    if (custProjProd.invoices != null)
                    {
                        try
                        {
                            CreateOrUpdateInvoice createOrUpdateInvoice = new CreateOrUpdateInvoice();
                            custProjProd.invoices = createOrUpdateInvoice.CreateOrUpdate(custProjProd.invoices, tempFolderPath, 
                                this.checkBoxUpdateEvenIfDelivered, 
                                this.checkBoxUpdateEvenIfDocumentPrinted, 
                                this.checkBoxUpdateYourRefWithProjectName,
                                this.checkBoxCopyProjectDescriptionAsText);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error : " + ex.Message);
                            File.AppendAllText(tempFolderPath + "ImportLog.txt", ex.Message + Environment.NewLine + ex + Environment.NewLine);
                            resultatOutput = resultatOutput + Environment.NewLine + "Inga ordrar har skapats/uppdaterats";
                        }
                    }
                    else
                    {
                        resultatOutput = resultatOutput + Environment.NewLine + "Inga ordrar har skapats/uppdaterats";
                    }
                }

                // Products
                if (this.checkBoxImportArticles.Checked)
                {
                    if (custProjProd.Products != null)
                    {
                        try
                        {
                            CreateOrUpdateArticles createOrUpdateArticles = new CreateOrUpdateArticles();
                            custProjProd.Products = createOrUpdateArticles.CreateOrUpdate(custProjProd.Products, tempFolderPath);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error : " + ex.Message);
                            File.AppendAllText(tempFolderPath + "ImportLog.txt", ex.Message + Environment.NewLine + ex + Environment.NewLine);
                            resultatOutput = resultatOutput + Environment.NewLine + "Inga artiklar har skapats/uppdaterats";
                        }
                    }
                    else
                    {
                        resultatOutput = resultatOutput + Environment.NewLine + "Inga artiklar har skapats/uppdaterats";
                    }
                }

                // Customers
                if (this.checkBoxImportCustomers.Checked)
                {
                    if (custProjProd.Customers != null)
                    {
                        try
                        {
                            CreateOrUpdateCustomers createOrUpdateCustomers = new CreateOrUpdateCustomers();
                            custProjProd.Customers = createOrUpdateCustomers.CreateOrUpdate(custProjProd.Customers, tempFolderPath);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Error : " + ex.Message);
                            File.AppendAllText(tempFolderPath + "ImportLog.txt", ex.Message + Environment.NewLine + ex + Environment.NewLine);
                            resultatOutput = resultatOutput + Environment.NewLine + "Inga kunder har skapats/uppdaterats";
                        }
                    }
                    else
                    {
                        resultatOutput = resultatOutput + Environment.NewLine + "Inga kunder har skapats/uppdaterats";
                    }
                }
                custProjProd.documentCreateDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                string exportFromVismajson = JsonConvert.SerializeObject(custProjProd, Formatting.Indented, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                string objectName = "exportFromMG";
                PutS3Object putS3Object = new PutS3Object();
                putS3Object.Run(objectName, exportFromVismajson);
            }
            catch (Exception ex)
            {                
                Console.WriteLine("Error : " + ex.Message);
                File.AppendAllText(tempFolderPath + "ImportLog.txt", DateTime.Now + " " + ex.Message + Environment.NewLine + ex + Environment.NewLine);
            }
            Api.AdkClose();
            string importLogText = File.ReadAllText(tempFolderPath + "ImportLog.txt");
            MessageBox.Show(DateTime.Now + Environment.NewLine + "Import fr�n MyGizmo klar! " + Environment.NewLine 
                + Environment.NewLine + "(Kolla importloggen f�r ev fel.)");
        }

        private void buttonDisplayInfo_Click(object sender, EventArgs e)
        {
            InfoForm infoForm = new InfoForm();
            infoForm.ShowDialog();
        }


        private void buttonExportera_Click(object sender, EventArgs e)
        {
            string tempFolderPath = ConfigurationManager.AppSettings["TempFolderPath"];
            try
            {
                Cursor.Current = Cursors.WaitCursor;
                PutS3Object putS3Object = new PutS3Object();

                // Get Visma integration version
                int major = 0;
                int minor = 0;
                Api.AdkGetVersion(ref major, ref minor);

                Console.WriteLine("Ansluter mot Visma");
                File.AppendAllText(tempFolderPath + "ExportLog.txt", "Create log" +
                Environment.NewLine + " " + DateTime.Now);
                File.WriteAllText(tempFolderPath + "ExportLog.txt", String.Empty);
                File.AppendAllText(tempFolderPath + "ExportLog.txt", "Visma integrationsversion: " + major + "." + minor +
                    Environment.NewLine);
                int lineNumber = (new System.Diagnostics.StackFrame(0, true)).GetFileLineNumber();

                string SysPat = ConfigurationManager.AppSettings["SysPat"];
                string SysFtg = ConfigurationManager.AppSettings["SysFtg"];

                Api.ADKERROR error = Api.AdkOpen(ref SysPat, ref SysFtg);
                CheckError("�ppna", error, "export");

                ReturnObjectOrder ReturnObjOrder = new ReturnObjectOrder();

                ExportFromVisma exportFromVisma = new ExportFromVisma();

                // Customers
                if (this.checkBoxExportCustomers.Checked)
                {
                    WriteCustomer writeCustomer = new WriteCustomer();
                    Dictionary<string, object> CustDic = ReturnObjOrder.CustomerDic;
                    CustDic = writeCustomer.wCustomer(tempFolderPath, this.datePickerExportFromDate);
                    List<object> CustomerList = new List<object>(CustDic.Values); //.Take(15)
                    string Customerjson = JsonConvert.SerializeObject(CustomerList, Formatting.Indented, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });

                    File.AppendAllText(tempFolderPath + "customers.json", "Skapa customers");
                    File.WriteAllText(tempFolderPath + "customers.json", String.Empty);
                    File.AppendAllText(tempFolderPath + "customers.json", Customerjson);
                    exportFromVisma.customers = CustomerList;
                }

                // Articles
                if (this.checkBoxExportArticles.Checked)
                {
                    WriteArticle writeArticle = new WriteArticle();
                    Dictionary<string, object> ArtDic = ReturnObjOrder.ArticleDic;
                    ArtDic = writeArticle.wArticle(tempFolderPath, this.datePickerExportFromDate);
                    List<object> ArtList = new List<object>(ArtDic.Values); //.Take(15)
                    string Artjson = JsonConvert.SerializeObject(ArtList, Formatting.Indented, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                    exportFromVisma.products = ArtList;

                    File.AppendAllText(tempFolderPath + "products.json", "Skapa products");
                    File.WriteAllText(tempFolderPath + "products.json", String.Empty);
                    File.AppendAllText(tempFolderPath + "products.json", Artjson);
                }

                // Projects
                if (this.checkBoxExportProjects.Checked)
                {
                    WriteProject writeProject = new WriteProject();
                    Dictionary<string, object> ProjectDic = ReturnObjOrder.ProjectDic;
                    ProjectDic = writeProject.wProject(tempFolderPath, this.datePickerExportFromDate);
                    List<object> ProjectList = new List<object>(ProjectDic.Values); //.Take(15)
                    string Projectjson = JsonConvert.SerializeObject(ProjectList, Formatting.Indented, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                    exportFromVisma.projects = ProjectList;

                    File.AppendAllText(tempFolderPath + "projects.json", "Skapa projects");
                    File.WriteAllText(tempFolderPath + "projects.json", String.Empty);
                    File.AppendAllText(tempFolderPath + "projects.json", Projectjson);
                }

                // Orders
                if (this.checkBoxExportOrders.Checked)
                {
                    WriteOrder writeOrder = new WriteOrder();
                    Dictionary<double, object> OrderDic = ReturnObjOrder.OrderDic;
                    OrderDic = writeOrder.wOrder(tempFolderPath, this.datePickerExportFromDate, this.checkBoxIncludeCancelled, this.checkBoxIncludeInvoiced);
                    List<object> OrderList = new List<object>(OrderDic.Values); //.Take(15)
                    string Orderjson = JsonConvert.SerializeObject(OrderList, Formatting.Indented, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });
                    exportFromVisma.order = OrderList;

                    File.AppendAllText(tempFolderPath + "order.json", "Skapa order");
                    File.WriteAllText(tempFolderPath + "order.json", String.Empty);
                    File.AppendAllText(tempFolderPath + "order.json", Orderjson);
                }

                exportFromVisma.documentCreateDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                string exportFromVismajson = JsonConvert.SerializeObject(exportFromVisma, Formatting.Indented, new JsonSerializerSettings { DefaultValueHandling = DefaultValueHandling.Ignore });

                File.AppendAllText(tempFolderPath + "exportFromVisma.json", "Skapa exportFromVisma");
                File.WriteAllText(tempFolderPath + "exportFromVisma.json", String.Empty);
                File.AppendAllText(tempFolderPath + "exportFromVisma.json", exportFromVismajson);

                //File.AppendAllText(tempFolderPath + "MyGizmoExportFromVisma.properties", "�ndra properties");
                //File.WriteAllText(tempFolderPath + "MyGizmoExportFromVisma.properties", String.Empty);
                //File.AppendAllText(tempFolderPath + "MyGizmoExportFromVisma.properties", "Datum = " + DateTime.Now);
                Api.AdkClose();
                string objectName = "exportFromVisma";
                putS3Object.Run(objectName, exportFromVismajson);                

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
                Console.WriteLine("Error : " + ex.Message);
                File.AppendAllText(tempFolderPath + "ExportLog.txt", DateTime.Now + " " + ex.Message + Environment.NewLine + ex + Environment.NewLine);
            }
            Api.AdkClose();
            MessageBox.Show("Export till MyGizmo klar!");
        }

        public static void CheckError(string id, Api.ADKERROR error, string importOrExport)
        {
            if (error.lRc != Api.ADKE_OK)
            {
                string tempFolderPath = ConfigurationManager.AppSettings["TempFolderPath"];
                String errortext = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                Console.WriteLine(errortext + " " + id);
                MessageBox.Show(errortext + " " + id);
                if (importOrExport == "export")
                {
                    File.AppendAllText(tempFolderPath + "ExportLog.txt", DateTime.Now + " " + errortext + " " + id + Environment.NewLine);
                }
                if (importOrExport == "import")
                {
                    File.AppendAllText(tempFolderPath + "ImportLog.txt", DateTime.Now + " " + errortext + " " + id + Environment.NewLine);
                }

            }
        }

        private void checkBoxExportOrders_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBoxExportOrders.Checked == true)
            {
                this.checkBoxIncludeCancelled.Enabled = true;
                this.checkBoxIncludeInvoiced.Enabled = true;
            }
            else
            {
                this.checkBoxIncludeCancelled.Enabled = false;
                this.checkBoxIncludeInvoiced.Enabled = false;
                this.checkBoxIncludeCancelled.Checked = false;
                this.checkBoxIncludeInvoiced.Checked = false;
            }
        }

        private void checkBoxImportInvoices_CheckedChanged(object sender, EventArgs e)
        {
            if (this.checkBoxImportInvoices.Checked == true)
            {
                this.checkBoxUpdateEvenIfDelivered.Enabled = true;
                this.checkBoxUpdateEvenIfDocumentPrinted.Enabled = true;
                this.checkBoxUpdateYourRefWithProjectName.Enabled = true;
                this.checkBoxCopyProjectDescriptionAsText.Enabled = true;
            }
            else
            {
                this.checkBoxUpdateEvenIfDelivered.Enabled = false;
                this.checkBoxUpdateEvenIfDocumentPrinted.Enabled = false;
                this.checkBoxUpdateYourRefWithProjectName.Enabled = false;
                this.checkBoxCopyProjectDescriptionAsText.Enabled = false;
                this.checkBoxUpdateEvenIfDelivered.Checked = false;
                this.checkBoxUpdateEvenIfDocumentPrinted.Checked = false;
                this.checkBoxUpdateYourRefWithProjectName.Checked = false;
                this.checkBoxCopyProjectDescriptionAsText.Checked = false;
            }
        }

    }
}