﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdkNet4Wrapper;
using Newtonsoft.Json;
using System.IO;


namespace MyGizmoVisma
{
    public class WriteArticle
    {
        public Dictionary<string, object> wArticle(string tempFolderPath, System.Windows.Forms.DateTimePicker datePickerExportFromDate)
        {
            Dictionary<string, object> ArticleDic = new Dictionary<string, object>();

            Api.ADKERROR error;
            int pData;
            pData = Api.AdkCreateData(Api.ADK_DB_ARTICLE);
            error = Api.AdkFirst(pData);

            while (error.lRc != Api.ADKE_EOF)
            {
                String Artnr = new String(' ', 16);
                Api.AdkGetStr(pData, Api.ADK_ARTICLE_NUMBER, ref Artnr, 16);
                
                if (!ArticleDic.ContainsKey(Artnr))
                {
                    Article ArticleObj = new Article();                    
                    ArticleObj.ErpProductID = Artnr;
                    ArticleObj.ProductName = Artnr;

                    int TimeStampInt = 0;
                    String TimeStampString = new String(' ', 11);
                    error = Api.AdkGetDate(pData, Api.ADK_ARTICLE_TIMESTAMP, ref TimeStampInt);
                    CheckError("Artnr: " + Artnr, error, tempFolderPath);
                    if (TimeStampInt != 0)
                    {
                        error = Api.AdkLongToDate(TimeStampInt, ref TimeStampString, 11);
                        CheckError("Artnr: " + Artnr, error, tempFolderPath);
                        ArticleObj.ChangeDate = TimeStampString + " 00:00:00";
                        if (datePickerExportFromDate.Checked)
                        {
                            string selectedDateAsString = datePickerExportFromDate.Value.ToString("yyyy-MM-dd");
                            int value = string.Compare(TimeStampString, selectedDateAsString);
                            if (value < 0)
                            {
                                error = Api.AdkNext(pData);
                                continue;
                            }
                        }
                    }
                    else
                    {
                        error = Api.AdkNext(pData);
                        continue;
                    }

                    String Artikelnamn = new String(' ', 60);
                    error = Api.AdkGetStr(pData, Api.ADK_ARTICLE_NAME, ref Artikelnamn, 60);
                    CheckError("Artnr: " + Artnr, error, tempFolderPath);
                    if (Artikelnamn != "") { ArticleObj.ProductDescription = Artikelnamn; }

                    double PrisUt = 0;
                    error = Api.AdkGetDouble(pData, Api.ADK_ARTICLE_PRICE, ref PrisUt);
                    CheckError("Artnr: " + Artnr, error, tempFolderPath);
                    ArticleObj.Price = Convert.ToString(PrisUt);

                    String Enhet = new String(' ', 4);
                    error = Api.AdkGetStr(pData, Api.ADK_ARTICLE_UNIT_CODE, ref Enhet, 4);
                    CheckError("Artnr: " + Artnr, error, tempFolderPath);
                    if (Enhet != "")
                    {
                        ArticleObj.UnitName = Enhet; ;
                    }

                    String ProductTypeID = new String(' ', 6);
                    error = Api.AdkGetStr(pData, Api.ADK_ARTICLE_GROUP, ref ProductTypeID, 6);
                    CheckError("Artnr: " + Artnr, error, tempFolderPath);
                    if (ProductTypeID != "")
                    {
                        ArticleObj.ProductTypeName = ProductTypeID;
                    }

                    String ProductDescription = new String(' ', 60);
                    error = Api.AdkGetStr(pData, Api.ADK_ARTICLE_NAME_X, ref ProductDescription, 60);
                    CheckError("Artnr: " + Artnr, error, tempFolderPath);                    

                    int StockGoods = 0;
                    error = Api.AdkGetBool(pData, Api.ADK_ARTICLE_STOCK_GOODS, ref StockGoods);
                    CheckError("Artnr: " + Artnr, error, tempFolderPath);
                    ArticleObj.StockGoods = StockGoods.ToString();

                    int inActive = 1;
                    error = Api.AdkGetBool(pData, Api.ADK_ARTICLE_INACTIVE, ref inActive);
                    CheckError("Artnr: " + Artnr, error, tempFolderPath);

                    if (inActive == 1)
                    {
                        ArticleObj.Active = "0";
                    }else
                    {
                        ArticleObj.Active = "1";
                    }
                    ArticleDic.Add(Artnr, ArticleObj);
                }
                error = Api.AdkNext(pData);
            }
            error = Api.AdkDeleteStruct(pData);
            return ArticleDic;

        }

        public static string GetUnit(String Enhetskod)
        {
            if (Enhetskod != "")
            { }

            Api.ADKERROR error;
            int pDataFind = 0;
            string EnhetText = Enhetskod;
            String EnhetskodResult = new String(' ', 4);
            pDataFind = Api.AdkCreateData(Api.ADK_DB_CODE_OF_UNIT);
            error = Api.AdkSetStr(pDataFind, Api.ADK_CODE_OF_UNIT_CODE, ref Enhetskod);
            error = Api.AdkFind(pDataFind);
            Api.AdkGetStr(pDataFind, Api.ADK_CODE_OF_UNIT_CODE, ref EnhetskodResult, 4);
            if (error.lRc != Api.ADKE_OK)
            {
                String errortext = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
            }
            else if (Enhetskod == EnhetskodResult)
            {
                EnhetText = new String(' ', 12);
                error = Api.AdkGetStr(pDataFind, Api.ADK_CODE_OF_UNIT_TEXT, ref EnhetText, 12);
            }

            error = Api.AdkDeleteStruct(pDataFind);
            return EnhetText;
        }

        public static string GetArticleGroup(String ArticleGroupKod)
        {
            if (ArticleGroupKod != "")
            { }

            Api.ADKERROR error;
            int pDataFind = 0;
            string ArticleGroupText = "";
            String ArticleGroupkodResult = new String(' ', 6);
            pDataFind = Api.AdkCreateData(Api.ADK_DB_CODE_OF_ARTICLE_GROUP);
            error = Api.AdkSetStr(pDataFind, Api.ADK_CODE_OF_ARTICLE_GROUP_CODE, ref ArticleGroupkodResult);
            error = Api.AdkFind(pDataFind);
            Api.AdkGetStr(pDataFind, Api.ADK_CODE_OF_ARTICLE_GROUP_CODE, ref ArticleGroupkodResult, 6);
            if (error.lRc != Api.ADKE_OK)
            {
                String errortext = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
            }
            else if (ArticleGroupKod == ArticleGroupkodResult)
            {
                ArticleGroupText = new String(' ', 25);
                error = Api.AdkGetStr(pDataFind, Api.ADK_CODE_OF_ARTICLE_GROUP_TEXT, ref ArticleGroupText, 25);
            }

            error = Api.AdkDeleteStruct(pDataFind);
            return ArticleGroupText;
        }

        public static void CheckError(string id, Api.ADKERROR error, string tempFolderPath)
        {
            if (error.lRc != Api.ADKE_OK)
            {
                String errortext = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                Console.WriteLine(errortext + " " + id);
                File.AppendAllText(tempFolderPath + "ExportLog.txt", DateTime.Now + " " + errortext + " " + id + Environment.NewLine +
       Environment.NewLine);

            }
        }


    }
}

