﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AdkNet4Wrapper;
using Newtonsoft.Json;
using System.IO;

namespace MyGizmoVisma
{
    class WriteOrder
    {

        public Dictionary<double, object> wOrder(string tempFolderPath, System.Windows.Forms.DateTimePicker datePickerExportFromDate, 
            System.Windows.Forms.CheckBox checkBoxIncludeCancelled, System.Windows.Forms.CheckBox checkBoxIncludeInvoiced)
        {

            Dictionary<double, object> OrderDic = new Dictionary<double, object>();

            Api.ADKERROR error;
            int pData;

            pData = Api.AdkCreateData(Api.ADK_DB_ORDER_HEAD);
            //error = Api.Adk(pData);
            error = Api.AdkFirst(pData);
            

            while (error.lRc != Api.ADKE_EOF)
            {
                customerOrders customerOrdersObj = new customerOrders();

                Double ErpOrderNumber = new Double();
                Api.AdkGetDouble(pData, Api.ADK_OOI_HEAD_DOCUMENT_NUMBER, ref ErpOrderNumber);
                
                if (!OrderDic.ContainsKey(ErpOrderNumber))
                {
                    int TimeStampInt = 0;
                    String TimeStampString = new String(' ', 11);
                    error = Api.AdkGetDate(pData, Api.ADK_OOI_HEAD_TIMESTAMP, ref TimeStampInt);
                    CheckError("ErpOrderNumber: " + ErpOrderNumber, error, tempFolderPath);
                    if (TimeStampInt != 0)
                    {
                        error = Api.AdkLongToDate(TimeStampInt, ref TimeStampString,11);
                        CheckError("ErpOrderNumber: " + ErpOrderNumber, error, tempFolderPath);
                        DateTime date = DateTime.ParseExact(TimeStampString, "yyyy-MM-dd", System.Globalization.CultureInfo.InvariantCulture);
                        customerOrdersObj.ChangeDate = date + " 00:00:00";
                        if (datePickerExportFromDate.Checked)
                        {
                            string selectedDateAsString = datePickerExportFromDate.Value.ToString("yyyy-MM-dd");
                            int value = string.Compare(TimeStampString, selectedDateAsString);
                            if (value < 0)
                            {
                                error = Api.AdkNext(pData);
                                continue;
                            }
                        }
                    }
                    else
                    {
                        error = Api.AdkNext(pData);
                        continue;
                    }

                    customerOrdersObj.ErpOrderNumber = ErpOrderNumber.ToString();

                    String ErpCustomerID = new String(' ', 16);
                    Api.AdkGetStr(pData, Api.ADK_OOI_HEAD_CUSTOMER_NUMBER, ref ErpCustomerID, 16);
                    customerOrdersObj.ErpCustomerID = ErpCustomerID;

                    String OrderRemark = new String(' ', 150);
                    Api.AdkGetStr(pData, Api.ADK_OOI_HEAD_LOCAL_REMARK, ref OrderRemark, 150);
                    customerOrdersObj.OrderRemark = OrderRemark;

                    String OurReference = new String(' ', 50);
                    Api.AdkGetStr(pData, Api.ADK_OOI_HEAD_OUR_REFERENCE_NAME, ref OurReference, 50);
                    customerOrdersObj.OurReference = OurReference;

                    String YourReference = new String(' ', 50);
                    Api.AdkGetStr(pData, Api.ADK_OOI_HEAD_CUSTOMER_REFERENCE_NAME, ref YourReference, 50); 
                    customerOrdersObj.YourReference = YourReference;

                    int DeliveryDateInt = 0;
                    String DeliveryDateString = new String(' ', 11);
                    error = Api.AdkGetDate(pData, Api.ADK_OOI_HEAD_DOCUMENT_DATE2, ref DeliveryDateInt);
                    CheckError("ErpOrderNumber: " + ErpOrderNumber, error, tempFolderPath);
                    if (DeliveryDateInt != 0)
                    {
                        error = Api.AdkLongToDate(DeliveryDateInt, ref DeliveryDateString, 11);
                        CheckError("ErpOrderNumber: " + ErpOrderNumber, error, tempFolderPath);
                        customerOrdersObj.DeliveryDate = DeliveryDateString + " 00:00:00";
                    }

                    int OrderDateInt = 0;
                    String OrderDateString = new String(' ', 11);
                    error = Api.AdkGetDate(pData, Api.ADK_OOI_HEAD_DOCUMENT_DATE1, ref OrderDateInt);
                    CheckError("ErpOrderNumber: " + ErpOrderNumber, error, tempFolderPath);
                    if (OrderDateInt != 0)
                    {
                        error = Api.AdkLongToDate(OrderDateInt, ref OrderDateString, 11);
                        CheckError("ErpOrderNumber: " + ErpOrderNumber, error, tempFolderPath);
                        customerOrdersObj.OrderDate = OrderDateString + " 00:00:00";
                    }

                    int Cancelled = 0;
                    error = Api.AdkGetBool(pData, Api.ADK_OOI_HEAD_DOCUMENT_CANCELLED, ref Cancelled);
                    CheckError("ErpOrderNumber: " + ErpOrderNumber, error, tempFolderPath);
                    customerOrdersObj.Cancelled = Cancelled.ToString();

                    int Printed = 0;
                    error = Api.AdkGetBool(pData, Api.ADK_OOI_HEAD_DOCUMENT_PRINTED, ref Printed);
                    CheckError("ErpOrderNumber: " + ErpOrderNumber, error, tempFolderPath);
                    customerOrdersObj.Printed = Printed.ToString();

                    int Deliverd = 0;
                    error = Api.AdkGetBool(pData, Api.ADK_OOI_HEAD_ORDER_DELIVERED, ref Deliverd);
                    CheckError("ErpOrderNumber: " + ErpOrderNumber, error, tempFolderPath);
                    customerOrdersObj.Deliverd = Deliverd.ToString();

                    double AmountExcludingVATInCurrency = 0;
                    Api.AdkGetDouble(pData, Api.ADK_OOI_HEAD_EXCLUDING_OF_VAT, ref AmountExcludingVATInCurrency);
                    customerOrdersObj.AmountExcludingVATInCurrency = AmountExcludingVATInCurrency.ToString();
                    //AmountExcludingVATIncurrency

                    double InvoiceNumber = 0;
                    Api.AdkGetDouble(pData, Api.ADK_OOI_HEAD_DOCUMENT_CONNECTION_NUMBER2, ref InvoiceNumber);
                    customerOrdersObj.InvoiceNumber = InvoiceNumber.ToString();

                    if (InvoiceNumber == 0)
                    {
                        customerOrdersObj.Invoiced = "0";
                    }
                    else
                    {
                        customerOrdersObj.Invoiced = "1";
                    }

                    if ((Cancelled == 0))
                    {
                        if ((customerOrdersObj.Invoiced == "0"))
                        {
                            OrderDic.Add(ErpOrderNumber, customerOrdersObj);
                        }
                        else
                        {
                            if ((customerOrdersObj.Invoiced == "1") && checkBoxIncludeInvoiced.Checked)
                            {
                                OrderDic.Add(ErpOrderNumber, customerOrdersObj);
                            }
                        }
                    }
                    else
                    {
                        if (checkBoxIncludeCancelled.Checked)
                        {
                            OrderDic.Add(ErpOrderNumber, customerOrdersObj);
                        }
                    }
                }
                error = Api.AdkNext(pData);
            }
            error = Api.AdkDeleteStruct(pData);
            return OrderDic;
        }

        public static void CheckError(string id, Api.ADKERROR error, string tempFolderPath)
        {

            if (error.lRc != Api.ADKE_OK)
            {
                String errortext = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                Console.WriteLine(errortext + " " + id);
                File.AppendAllText(tempFolderPath + "ExportLog.txt", DateTime.Now + " " + errortext + " " + id + Environment.NewLine +
       Environment.NewLine);

            }
        }
    }
}
