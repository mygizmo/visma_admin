﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AdkNet4Wrapper;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;

namespace MyGizmoVisma
{
    class WriteProject
    {
        public Dictionary<string, object> wProject(string tempFolderPath, System.Windows.Forms.DateTimePicker datePickerExportFromDate)
        {
            Dictionary<string, object> ProjectDic = new Dictionary<string, object>();

            Api.ADKERROR error;
            int pData;
            pData = Api.AdkCreateData(Api.ADK_DB_PROJECT);

            error = Api.AdkFirst(pData);

            while (error.lRc != Api.ADKE_EOF)
            {
                String projectNr = new String(' ', 10);
                error = Api.AdkGetStr(pData, Api.ADK_PROJECT_CODE_OF_PROJECT, ref projectNr, 10);
                
                if (!ProjectDic.ContainsKey(projectNr))
                {
                    ProjectExport ProjectObj = new ProjectExport();                    
                    CheckError("projektnummer: " + projectNr, error, tempFolderPath);
                    ProjectObj.ErpProjectID = projectNr;

                    int TimeStampInt = 0;
                    String TimeStampString = new String(' ', 11);
                    error = Api.AdkGetDate(pData, Api.ADK_PROJECT_TIMESTAMP, ref TimeStampInt);
                    CheckError("Projektnummer: " + projectNr, error, tempFolderPath);
                    if (TimeStampInt != 0)
                    {
                        error = Api.AdkLongToDate(TimeStampInt, ref TimeStampString, 11);
                        CheckError("Projektnummer: " + projectNr, error, tempFolderPath);
                        ProjectObj.ChangeDate = TimeStampString + " 00:00:00";
                        if (datePickerExportFromDate.Checked)
                        {
                            string selectedDateAsString = datePickerExportFromDate.Value.ToString("yyyy-MM-dd");
                            int value = string.Compare(TimeStampString, selectedDateAsString);
                            if (value < 0)
                            {
                                error = Api.AdkNext(pData);
                                continue;
                            }
                        }
                    }
                    else
                    {
                        error = Api.AdkNext(pData);
                        continue;
                    }

                    String projekctKundNr = new String(' ', 16);
                    error = Api.AdkGetStr(pData, Api.ADK_PROJECT_CUSTOMER_NUMBER, ref projekctKundNr, 16);
                    CheckError("Projektnummer: " + projectNr, error, tempFolderPath);
                    if (projekctKundNr != "") { ProjectObj.ErpCustomerID = projekctKundNr; }

                    String projekctKundName = new String(' ', 50);
                    error = Api.AdkGetStr(pData, Api.ADK_PROJECT_CUSTOMER_NAME, ref projekctKundName, 50);
                    CheckError("Projektnummer: " + projectNr, error, tempFolderPath);
                    if (projekctKundName != "") { ProjectObj.CustomerName = projekctKundName; }

                    String ProjectName = new String(' ', 50);
                    error = Api.AdkGetStr(pData, Api.ADK_PROJECT_NAME, ref ProjectName, 50);
                    CheckError("Projektnummer: " + projectNr, error, tempFolderPath);
                    if (ProjectName != "") { ProjectObj.ProjectName = ProjectName; }

                    String Description = new String(' ', 60);
                    error = Api.AdkGetStr(pData, Api.ADK_PROJECT_NOTE1, ref Description, 60);
                    CheckError("Projektnummer: " + projectNr, error, tempFolderPath);
                    if (Description != "") { ProjectObj.Description = Description; }


                    int ProjectStartDatumInt = 0;
                    String ProjectStartDatumString = new String(' ', 11);
                    error = Api.AdkGetDate(pData, Api.ADK_PROJECT_DATE_OF_BEGINNING, ref ProjectStartDatumInt);
                    CheckError("Projektnummer: " + projectNr, error, tempFolderPath);
                    error = Api.AdkLongToDate(ProjectStartDatumInt, ref ProjectStartDatumString, 11);
                    CheckError("Projektnummer: " + projectNr, error, tempFolderPath);
                    if (ProjectStartDatumString != "") { ProjectObj.StartDate = Convert.ToDateTime(ProjectStartDatumString); }

                    int ProjectEndDatumInt = 0;
                    String ProjectEndDatumString = new String(' ', 11);
                    error = Api.AdkGetDate(pData, Api.ADK_PROJECT_DATE_OF_END, ref ProjectEndDatumInt);
                    CheckError("Projektnummer: " + projectNr, error, tempFolderPath);
                    error = Api.AdkLongToDate(ProjectEndDatumInt, ref ProjectEndDatumString, 11);
                    CheckError("Projektnummer: " + projectNr, error, tempFolderPath);
                    if (ProjectEndDatumString != "") { ProjectObj.EndDate = Convert.ToDateTime(ProjectEndDatumString); }

                    //---------
                    int inActive = 1;
                    error = Api.AdkGetBool(pData, Api.ADK_PROJECT_CLOSED, ref inActive);
                    CheckError("Projektnummer: " + projectNr, error, tempFolderPath);

                    
                    if (inActive == 1)
                    {
                        ProjectObj.Active = "0";
                    }else
                    {
                        ProjectObj.Active = "1";
                    }
                    ProjectDic.Add(projectNr, ProjectObj);

                }
                error = Api.AdkNext(pData);
            }

            error = Api.AdkDeleteStruct(pData);
            return ProjectDic;

        }


        public static void CheckError(string id, Api.ADKERROR error, string tempFolderPath)
        {

            if (error.lRc != Api.ADKE_OK)
            {
                String errortext = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                Console.WriteLine(errortext + " " + id);
                File.AppendAllText(tempFolderPath + "ExportLog.txt", DateTime.Now + " " + errortext + " " + id + Environment.NewLine +
       Environment.NewLine);

            }
        }



    }
}
