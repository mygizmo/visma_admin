﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdkNet4Wrapper;
using Newtonsoft.Json;
using System.IO;

namespace MyGizmoVisma
{
    public class WriteCustomer
    {
        public Dictionary<string, object> wCustomer(string tempFolderPath, System.Windows.Forms.DateTimePicker datePickerExportFromDate)
        {
            Dictionary<string, object> CustomerDic = new Dictionary<string, object>();

            Api.ADKERROR error;
            int pData;
            pData = Api.AdkCreateData(Api.ADK_DB_CUSTOMER);
            error = Api.AdkFirst(pData);

            while (error.lRc != Api.ADKE_EOF)
            {
                String Kundnr = new String(' ', 16);
                error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_NUMBER, ref Kundnr, 16);                
                if (!CustomerDic.ContainsKey(Kundnr))
                {
                    Customer CustomerObj = new Customer();                    
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);                    
                    CustomerObj.ErpCustomerID = Kundnr;

                    int TimeStampInt = 0;
                    String TimeStampString = new String(' ', 11);
                    error = Api.AdkGetDate(pData, Api.ADK_CUSTOMER_TIMESTAMP, ref TimeStampInt);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                    if (TimeStampInt != 0)
                    {
                        error = Api.AdkLongToDate(TimeStampInt, ref TimeStampString, 11);
                        CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                        CustomerObj.ChangeDate = TimeStampString + " 00:00:00";
                        if (datePickerExportFromDate.Checked)
                        {
                            string selectedDateAsString = datePickerExportFromDate.Value.ToString("yyyy-MM-dd");
                            int value = string.Compare(TimeStampString, selectedDateAsString);
                            if (value < 0)
                            {
                                error = Api.AdkNext(pData);
                                continue;
                            }
                        }
                    }
                    else
                    {
                        error = Api.AdkNext(pData);
                        continue;
                    }

                    String Kundnamn = new String(' ', 50);
                    error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_NAME, ref Kundnamn, 50);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                    if (Kundnamn != "") { CustomerObj.Name = Kundnamn; }

                    String VarRef = new String(' ', 10);
                    error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_SELLER, ref VarRef, 10);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);  
                    
                    String AdressLine1 = new String(' ', 35);
                    error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_MAILING_ADDRESS, ref AdressLine1, 35);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                    if (AdressLine1 != "") { CustomerObj.AdressLine1 = AdressLine1; }

                    String AdressLine2 = new String(' ', 35);
                    error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_MAILING_ADDRESS2, ref AdressLine2, 35);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                    if (AdressLine2 != "") { CustomerObj.AdressLine2 = AdressLine2; }

                    String AdressLine3 = new String(' ', 35);
                    error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_VISITING_ADDRESS, ref AdressLine3, 35);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                    if (AdressLine3 != "") { CustomerObj.AdressLine3 = AdressLine3; }

                    String PostalCode = new String(' ', 12);
                    error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_ZIPCODE, ref PostalCode, 12);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                    if (PostalCode != "") { CustomerObj.PostalCode = PostalCode; }

                    String City = new String(' ', 24);
                    error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_CITY, ref City, 24);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                    if (City != "") { CustomerObj.City = City; }

                    String Country = new String(' ', 24);
                    error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_COUNTRY, ref Country, 24);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                    if (Country != "") { CustomerObj.Country = Country; }

                    String PhoneNumber = new String(' ', 20);
                    error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_TELEPHONE, ref PhoneNumber, 20);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                    if (PhoneNumber != "") { CustomerObj.PhoneNumber = PhoneNumber; }

                    String OrganisationNumber = new String(' ', 14);
                    error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_ORGANISATION_NUMBER, ref OrganisationNumber, 14);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                    if (OrganisationNumber != "") { CustomerObj.OrganisationNumber = OrganisationNumber; }

                    String Email = new String(' ', 70);
                    error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_EMAIL, ref Email, 70);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                    if (Email != "") { CustomerObj.Email = Email; }

                    String ContactPerson = new String(' ', 50);
                    error = Api.AdkGetStr(pData, Api.ADK_CUSTOMER_REFERENCE, ref ContactPerson, 50);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);
                    if (ContactPerson != "") { CustomerObj.ContactPerson = ContactPerson; }

                    int inaktiv = 1;
                    error = Api.AdkGetBool(pData, Api.ADK_CUSTOMER_INACTIVE, ref inaktiv);
                    CheckError("Kundnr: " + Kundnr, error, tempFolderPath);                    
                    if (inaktiv == 1)
                    {
                        CustomerObj.Active = "0";
                    }
                    else
                    {
                        CustomerObj.Active = "1";
                        
                    }
                    CustomerDic.Add(Kundnr, CustomerObj);
                }
                error = Api.AdkNext(pData);
            }

            error = Api.AdkDeleteStruct(pData);
            return CustomerDic;

        }


        public static string GetCat(String KundCatkod)
        {
            if (KundCatkod != "")
            { }

            Api.ADKERROR error;
            int pDataFind = 0;
            string KundCatText = KundCatkod;
            String KundCatkodResult = new String(' ', 4);
            pDataFind = Api.AdkCreateData(Api.ADK_DB_CODE_OF_CUSTOMER_CATEGORY);
            error = Api.AdkSetStr(pDataFind, Api.ADK_CODE_OF_CUSTOMER_CATEGORY_CODE, ref KundCatkod);
            error = Api.AdkFind(pDataFind);
            Api.AdkGetStr(pDataFind, Api.ADK_CODE_OF_CUSTOMER_CATEGORY_CODE, ref KundCatkodResult, 4);
            if (error.lRc != Api.ADKE_OK)
            {
                String errortext = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
            }
            else if (KundCatkod == KundCatkodResult)
            {
                KundCatText = new String(' ', 25);
                error = Api.AdkGetStr(pDataFind, Api.ADK_CODE_OF_CUSTOMER_CATEGORY_TEXT, ref KundCatText, 25);
            }

            error = Api.AdkDeleteStruct(pDataFind);
            return KundCatText;
        }

        public static string GetVarRef(String KundCatkod)
        {
            if (KundCatkod != "")
            { }

            Api.ADKERROR error;
            int pDataFind = 0;
            string KundCatText = KundCatkod;
            String KundCatkodResult = new String(' ', 10);
            pDataFind = Api.AdkCreateData(Api.ADK_DB_CODE_OF_SELLER);
            error = Api.AdkSetStr(pDataFind, Api.ADK_CODE_OF_SELLER_SIGN, ref KundCatkod);
            error = Api.AdkFind(pDataFind);
            Api.AdkGetStr(pDataFind, Api.ADK_CODE_OF_SELLER_SIGN, ref KundCatkodResult, 10);
            if (error.lRc != Api.ADKE_OK)
            {
                String errortext = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
            }
            else if (KundCatkod == KundCatkodResult)
            {
                KundCatText = new String(' ', 25);
                error = Api.AdkGetStr(pDataFind, Api.ADK_CODE_OF_SELLER_NAME, ref KundCatText, 25);
            }

            error = Api.AdkDeleteStruct(pDataFind);
            return KundCatText;
        }

        public static void CheckError(string id, Api.ADKERROR error, string tempFolderPath)
        {
            if (error.lRc != Api.ADKE_OK)
            {
                String errortext = new String(' ', 200);
                int errtype = (int)Api.ADK_ERROR_TEXT_TYPE.elRc;
                Api.AdkGetErrorText(ref error, errtype, ref errortext, 200);
                Console.WriteLine(errortext + " " + id);
                File.AppendAllText(tempFolderPath + "ExportLog.txt", DateTime.Now + " " + errortext + " " + id + Environment.NewLine +
       Environment.NewLine);

            }
        }



    }
}
