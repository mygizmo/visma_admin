﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyGizmoVisma
{
    class customerOrders
    {

        public string ErpCustomerID { get; set; }
        public string ErpOrderNumber { get; set; }
        public string OrderRemark { get; set; }
        public string OurReference { get; set; }
        public string YourReference { get; set; }
        public string Status { get; set; }
        public string Active { get; set; }
        public string DeliveryDate { get; set; }        
        public string OrderDate { get; set; }
        public string ChangeDate { get; set; }
        public string Deliverd { get; set; }
        public string Printed { get; set; }
        public string Invoiced { get; set; }
        public string Cancelled { get; set; }
        public string AmountExcludingVATInCurrency { get; set; }
        public string InvoiceNumber { get; set; }

    }
}


