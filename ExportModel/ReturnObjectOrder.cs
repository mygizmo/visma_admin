﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGizmoVisma
{
    public class ReturnObjectOrder
    {            
        public Dictionary<string, object> CustomerDic { get; set; }
        public Dictionary<string, object> ArticleDic { get; set; }
        public Dictionary<string, object> ProjectDic { get; set; }
        public Dictionary<double, object> OrderDic { get; set; }
    }
}
