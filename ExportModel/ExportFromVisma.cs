﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MyGizmoVisma
{
    class ExportFromVisma
    {
        public string documentCreateDate { get; set; }
        public List<object> customers { get; set; }
        public List<object> projects { get; set; }
        public List<object> products { get; set; }
        public List<object> order { get; set; }
    }
}
