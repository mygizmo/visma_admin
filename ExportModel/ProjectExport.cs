﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyGizmoVisma
{
    class ProjectExport
    {        
        public string CustomerID { get; set; }  
        public string ErpCustomerID { get; set; }        
        public string CustomerName { get; set; }
        public int CustomerObjectID { get; set; } 
        public string Description { get; set; } 
        public DateTime EndDate { get; set; } 
        public string ErpProjectID { get; set; } 
        public string ObjectAdressLine1 { get; set; } 
        public string ObjectAdressLine2 { get; set; } 
        public string ObjectCity { get; set; } 
        public string ObjectContactPerson { get; set; } 
        public string ObjectDescription { get; set; } 
        public string ObjectName { get; set; } 
        public string ObjectPostalCode { get; set; } 
        public string ProjectName { get; set; } 
        public DateTime StartDate { get; set; } 
        public int id { get; set; }
        public string Active { get; set; }
        public string ChangeDate { get; set; }

    }
}
